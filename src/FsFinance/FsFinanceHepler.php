<?php

namespace Fstar\Client\FsFinance;

use Fstar\Client\LogService;
use GuzzleHttp\Client;

class FsFinanceHepler {
    private $finance_user_no = null;
    private $finance_token = null;
    private $finance_host = null;
    private $finance_db_domain = null;
    private $log = null;

    function __construct($config = []) {
        $config = empty($config) ? config('fstar-client') : $config;
        $this->finance_host = data_get($config, 'fs_finance_host');
        $this->finance_token = data_get($config, 'fs_finance_token');
        $this->finance_user_no = data_get($config, 'fs_finance_user_no');
        $this->finance_db_domain = data_get($config, 'fs_finance_db_domain');
        $this->log = new LogService('fs_finance', data_get($config, 'fs_finance_write_log', true));
    }

    protected function post($path, $data) {
        $now = time();
        $url = "{$this->finance_host}{$path}";
        $param_str = is_string($data) ? $data : json_encode($data);
        $headers = $this->getHeaders($now, $param_str);
        $header_str = json_encode($headers);
        $this->log->info("Req url:{$url} headers:{$header_str} params:{$param_str}");
        try {
            $response = (new Client())->request('POST', $url, [
                'headers'         => $headers,
                'timeout'         => 60,
                'connect_timeout' => 5,
                'verify'          => false,
                'body'            => $param_str
            ]);
            $content = $response->getBody()->getContents();
            $this->log->info("Resp body:{$content}");
            if(!is_array($content)) {
                $content = json_decode($content, true);
            }
            if($content['type'] == 'success') {
                return $content['result'];
            } else {
                throw new FsFinanceException($content['message']);
            }
        } catch(\Exception $ex) {
            throw new FsFinanceException("财务一体化平台接口请求失败,{$ex->getMessage()}");
        }
    }

    private function getHeaders($now, $data) {
        $auth = $this->finance_user_no.':'.md5("{$now}{$this->finance_token}{$data}");
        return ['Content-Type' => 'application/json', 'Authorization' => $auth, 'Date' => $now, 'TenantDomain' => $this->finance_db_domain];
    }
}
