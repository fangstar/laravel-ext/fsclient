<?php

namespace Fstar\Client\FsFinance;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\FsFinance\FsFinanceService
 *
 * @method static FsFinanceService newInstanse()
 * @method static partnerSave(array $params)
 * @method static partnerDel(string $src_object)
 * @method static deptSave(array $params)
 * @method static deptDel(string $src_object, boolean $include_child)
 * @method static empSave(array $params)
 * @method static empDel(string $src_object)
 */
class FsFinanceAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_fs_finance;
    }
}