<?php

namespace Fstar\Client\FsFinance;

use Fstar\Client\LogService;
use GuzzleHttp\Client;

class FsFinanceService extends FsFinanceHepler {
    function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * 添加合作伙伴信息
     *
     * @param $params
     * [
     * {"field":"partner_name","name":"合作方全称","validator":"required|max:63"},
     * {"field":"partner_short_name","name":"合作方简称","validator":"required|max:31"},
     * {"field":"headquarters","name":"合作方总部简称","validator":"max:31"},
     * {"field":"is_project","name":"是否是项目方","validator":"in:0,1"},
     * {"field":"is_distribution","name":"是否是分销方","validator":"in:0,1"},
     * {"field":"is_payer","name":"是否是付款方","validator":"in:0,1"},
     * {"field":"is_channel","name":"是否是渠道方","validator":"in:0,1"},
     * {"field":"is_payee","name":"是否是收款方","validator":"in:0,1"},
     * {"field":"is_franchisee","name":"是否是加盟方","validator":"in:0,1"},
     * {"field":"is_other","name":"是否是其他","validator":"in:0,1"},
     * {"field":"is_valid","name":"是否有效","validator":"in:1,2"},
     * {"field":"tax_no","name":"统一社会信用代码","validator":"max:31"},
     * {"field":"tax_rate","name":"税率","validator":"digits_between:0,100"},
     * {"field":"address","name":"地址","validator":"max:63"},
     * {"field":"contact","name":"联系方式","validator":"max:31"},
     * {"field":"bank_no","name":"银行账号","validator":"max:31"},
     * {"field":"bank_name","name":"银行名称","validator":"max:63"},
     * {"field":"src_object","name":"来源对象值/第三方记录ID","validator":"max:255"}
     * ]
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function partnerSave($params) {
        $now = time();
        $path = '/openapi/base/partner/save';
        return $this->post($path, $params);
    }

    /**
     * 删除合作伙伴信息
     *
     * @param $src_object string 来源对象值/第三方记录ID max:255
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function partnerDel($src_object) {
        $now = time();
        $path = '/openapi/base/partner/del';
        $params = ['src_object' => $src_object];
        return $this->post($path, $params);
    }

    /**
     * 保存组织机构
     *
     * @param $params
     * [
     * {"field":"dept_name","name":"部门名称","validator":"required|max:255"},
     * {"field":"dept_name_spell","name":"部门简拼","validator":"max:255"},
     * {"field":"dept_code","name":"部门编码","validator":"required|max:255"},
     * {"field":"dept_type","name":"部门类型","validator":"required|integer"},
     * {"field":"src_object","name":"来源对象/第三方组织机构ID","validator":"required|max:255"},
     * {"field":"src_p_object","name":"来源父对象/第三方组织机构PID","validator":"required|max:255"}
     * ]
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function deptSave($params) {
        $now = time();
        $path = '/openapi/base/dept/save';
        return $this->post($path, $params);
    }

    /**
     * 删除组织机构
     *
     * @param $src_object    string 来源对象值/第三方记录ID max:255
     * @param $include_child boolean 是否包含子部门
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function deptDel($src_object, $include_child = false) {
        $now = time();
        $path = '/openapi/base/dept/del';
        $params = ['src_object' => $src_object, 'include_child' => $include_child];
        return $this->post($path, $params);
    }

    /**
     * 保存员工信息
     *[
     * {"field":"emp_no","name":"员工工号","validator":"required|max:31"},
     * {"field":"emp_name","name":"员工姓名","validator":"required|max:31"},
     * {"field":"mobile","name":"手机号","validator":"required|max:63"},
     * {"field":"id_num","name":"身份证号","validator":"required|max:31"},
     * {"field":"gender","name":"性别","validator":"required|in:0,1"},
     * {"field":"bank_card_no","name":"银行卡号","validator":"max:31"},
     * {"field":"bank_card_no2","name":"银行卡号2","validator":"max:31"},
     * {"field":"is_on_job","name":"是否在职","validator":"in:1,2"},
     * {"field":"position_id","name":"职务","validator":"required|integer"},
     * {"field":"workplace_no","name":"工作场所编码","validator":"size:6"},
     * {"field":"emp_type","name":"员工类型","validator":"required"},
     * {"field":"src_object","name":"来源对象","validator":"required|max:255"},
     * {"field":"src_dept_object","name":"第三方部门ID","validator":"max:63"},
     * {"field":"src_partner_object","name":"第三方合作方ID","validator":"max:63"},
     * {"field":"emp_name_spell","name":"员工姓名拼写","validator":"max:31"},
     * {"field":"emp_alias","name":"别名","validator":"max:31"},
     * {"field":"emp_entry_date","name":"入职时间","validator":"nullable|integer"},
     * {"field":"emp_dimission_date","name":"第三方部门ID","validator":"nullable|integer"}
     * ]
     *
     * @param $params
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function empSave($params) {
        $now = time();
        $path = '/openapi/base/emp/save';
        return $this->post($path, $params);
    }

    /**
     * 删除员工信息
     *
     * @param $src_object    string 来源对象值/第三方记录ID max:255
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function empDel($src_object) {
        $now = time();
        $path = '/openapi/base/emp/del';
        $params = ['src_object' => $src_object];
        return $this->post($path, $params);
    }

    /**
     * 保存新房成交报告
     * [
     * {"field":"src_object","name":"报告来源ID","validator":"required"},
     * {"field":"src_no","name":"员工来源成交记录编码","validator":"required|max:63"},
     * {"field":"is_history","name":"报告类型","validator":"required|in:0,1"},
     * {"field":"src_estate_object","name":"源楼盘ID","validator":"required"},
     * {"field":"estate_name","name":"楼盘","validator":"required|max:63"},
     * {"field":"estate_name","name":"楼盘","validator":"required|max:63"},
     * {"field":"city_id","name":"城市id","validator":"required"},
     * {"field":"build_name","name":"栋座","validator":"required|max:63"},
     * {"field":"unit_name","name":"单元","validator":"required|max:31"},
     * {"field":"room_no","name":"房号","validator":"required|max:31"},
     * {"field":"customer_name_1","name":"客户姓名1","validator":"required|max:31"},
     * {"field":"customer_name_2","name":"客户姓名2","validator":"required|max:31"},
     * {"field":"report_status","name":"报告状态","validator":"required"},
     * {"field":"progress_status","name":"执行进度","validator":"required"},
     * {"field":"deal_date","name":"成交日期","validator":"required|integer"},
     * {"field":"deal_amount","name":"成交金额","validator":"required|decimal|min:0.01"},
     * {"field":"estimate_receivable_amount","name":"预计应收金额","validator":"required|decimal|min:0.01"},
     * {"field":"deal_bonus","name":"成交专属奖励","validator":"required|decimal|min:0.01"},
     * {"field":"src_deal_emp_object","name":"源成交人ID","validator":"required"},
     * {"field":"src_deal_emp_name","name":"成交人姓名","validator":"nullable|max:31"},
     * {"field":"deal_position_id","name":"成交人职级ID","validator":"nullable"},
     * {"field":"src_deal_dept_object","name":"源成交部门id","validator":"nullable"},
     * {"field":"src_partner_object","name":"源合作方id","validator":"nullable"}
     * ]
     *
     * @param $params
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function nhReportSave($params) {
        $now = time();
        $path = '/openapi/nh/nh-report/save';
        return $this->post($path, $params);
    }

    /**
     * 删除成交报告.
     *
     * @param $src_object string 来源对象值/第三方记录ID max:255
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function nhReportDel($src_object) {
        $now = time();
        $path = '/openapi/nh/nh-report/del';
        $params = ['src_object' => $src_object];
        return $this->post($path, $params);
    }

    /**
     * 保存新房楼盘.
     * [
     * {"field":"city_id","name":"城市ID","validator":"required"},
     * {"field":"estate_name","name":"楼盘名称","validator":"required|max:63"},
     * {"field":"src_object","name":"来源对象/第三方组织机构ID","validator":"required|max:255"},
     * {"field":"delete_flag","name":"删除标志","validator":"required|in:0,1"},
     * ]
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function nhEstateSave($params) {
        $now = time();
        $path = '/openapi/nh/nh-estate/save';
        return $this->post($path, $params);
    }

    /**
     * 保存新房楼盘.
     * [
     * {"field":"src_object","name":"来源对象/第三方组织机构ID","validator":"required|max:255"},
     * ]
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function nhEstateDel($src_object) {
        $now = time();
        $path = '/openapi/nh/nh-estate/del';
        $params = ['src_object' => $src_object];
        return $this->post($path, $params);
    }

    /**
     * 保存新房报告附件.
     * [
     * {"field":"src_object","name":"来源对象/第三方组织机构ID","validator":"required|max:255"},
     * {"field":"src_report_object","name":"来源对象/第三方组织机构成交报告ID","validator":"required|max:255"},
     * {"field":"attach_url","name":"附件路径","validator":"required|max:510"},
     * {"field":"attach_name","name":"附件名称","validator":"null|max:120"},
     * {"field":"attach_type","name":"附件类型","validator":"required"}
     * ]
     *
     * @param $params
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function nhReportAttachSave($params) {
        $now = time();
        $path = '/openapi/nh/nh-report-attach/save';
        return $this->post($path, $params);
    }

    /**
     * 删除新房报告附件.
     * [
     * {"field":"src_object","name":"来源对象/第三方组织机构ID","validator":"required|max:255"},
     * ]
     *
     * @return mixed
     * @throws FsFinanceException
     */
    public function nhReportAttachDel($src_object) {
        $now = time();
        $path = '/openapi/nh/nh-report-attach/del';
        $params = ['src_object' => $src_object];
        return $this->post($path, $params);
    }
}
