<?php

namespace Fstar\Client;

class Constants {
    const conf_name = 'fstar-client';
    const SYS_EMP_ID = -99;
    const DEL_NO = 0;

    const lib_msgc = 'fstar_msgc';
    const lib_msgc_im = 'fstar_msgc_im';
    const lib_msgc_captcha = 'fstar_msgc_captcha';
    const lib_hana = 'fstar_hanac';
    const lib_fspay = 'fstar_fspay';
    const lib_protyle = 'fstar_protyle';
    const lib_eagleeye = 'fstar_eagleeye';
    const lib_mongo_svc = 'fstar_mongo_svc';
    const lib_erp_base = 'fstar_erp_base';
    const lib_fs_finance = 'fstar_fs_finance';
}