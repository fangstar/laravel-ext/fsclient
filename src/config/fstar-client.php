<?php

return [
    //base库连接 用于hana接口
    'db_conn_base'          => env('DB_CONN_BASE', 'mysql'),
    'hana_write_log'        => env('HANA_WRITE_LOG', true),
    //消息中心配置
    'msgc_host'             => env('MSGC_HOST', 'http://msgc.fstar.com:81'),
    'msgc_token'            => env('MSGC_TOKEN'),
    'msgc_user_no'          => env('MSGC_USER_NO'),
    'msgc_im_app_key'       => env('MSGC_IM_APP_KEY'),
    'msgc_im_app_cate'      => env('MSGC_IM_APP_CATE', 'starmate'),
    'msgc_write_log'        => env('MSGC_WRITE_LOG', true),
    //protyle 配置
    'protyle_url'           => env('PROTYLE_URL', 'http://protyle.fstar.com:81/'),
    'protyle_domain'        => env('PROTYLE_DOMAIN', 'ERP'),
    'protyle_catelog'       => env('PROTYLE_CATALOG', 'global'),
    'protyle_write_log'     => env('PROTYLE_WRITE_LOG', true),
    //eagleeye 配置
    'eagle_eye_host'        => env('EAGLE_EYE_HOST', 'http://eagleeye.fstar.com:81'),
    'eagle_eye_token'       => env('EAGLE_EYE_TOKEN'),
    'eagle_eye_user_no'     => env('EAGLE_EYE_USER_NO'),
    'eagle_eye_ret_data'    => env('EAGLE_EYE_RET_DATA', false),
    'eagle_eye_write_log'   => env('EAGLE_EYE_WRITE_LOG', true),
    //mongodb 服务配置
    'mongodb_svc_url'       => env('MONGODB_SVC_URL', 'http://logsys.fstar.com:81/'),
    'mongodb_svc_write_log' => env('MONGODB_SVC_WRITE_LOG', false),
    //ERP BASE 服务配置
    'erp_base_url'          => env('ERP_BASE_URL', 'http://base.fstar.com:81/'),
    'erp_base_write_log'    => env('ERP_BASE_WRITE_LOG', true),
    //FSPAY 服务配置
    'fspay_url'             => env('FSPAY_URL', 'http://fspay.fstar.com:81/'),
    'fspay_token'           => env('FSPAY_TOKEN'),
    'fspay_user_no'         => env('FSPAY_USER_NO'),
    'fspay_write_log'       => env('FSPAY_WRITE_LOG', true),
    //FS FINANCE
    'fs_finance_host'       => env('FS_FINANCE_HOST', 'http://finance.fstar.com:81'),
    'fs_finance_token'      => env('FS_FINANCE_TOKEN'),
    'fs_finance_user_no'    => env('FS_FINANCE_USER_NO'),
    'fs_finance_write_log'  => env('FS_FINANCE_WRITE_LOG', true),
    'fs_finance_db_domain'  => env('FS_FINANCE_DB_DOMAIN'),
];
