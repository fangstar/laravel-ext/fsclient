<?php

namespace Fstar\Client\Mongo;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\Mongo\MongoService
 *
 * @method static MongoService newInstanse()
 * @method static taskLogRecordFindById(string $id)
 * @method static taskLogAddRecord(array $record)
 * @method static taskLogUpdRecord(array $record)
 * @method static taskLogFindRecord(array $params)
 * @method static taskLogAddLog(array $log)
 * @method static taskLogBatchAddLog(array $logs)
 * @method static taskLogFind(string $task_record_id)
 * @method static visitLogFind(string $mobile, int $min_time, int $cnt)
 * @method static svcFlowLogAdd(string $svc_flow_instance_id, string $svc_flow_node_name, string $log_content, int $log_order, string $log_type = 'info')
 * @method static svcFlowLogFind(string $svc_flow_instance_id)
 * @method static fsAgentScoreFindByAgentId(string $agent_id)
 * @method static fsAgentScoreUpsert(string|array $params)
 * @method static fsRecommendFindByDicId(string $dic_id)
 * @method static fsRecommendFindByHouseType(string $house_type)
 * @method static fsRecommendTruncate()
 * @method static fsRecommendUpsert(string|array $params)
 * @method static phoneBrowseFind(string|array $params)
 * @method static phoneBrowseAdd(string|array $params)
 */
class MongoAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_mongo_svc;
    }
}