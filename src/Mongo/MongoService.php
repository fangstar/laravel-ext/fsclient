<?php

namespace Fstar\Client\Mongo;

class MongoService extends MongoHelper {
    public function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    public function taskLogRecordFindById($id) {
        $path = 'erp-task/record/find-by-id';
        return $this->get($path, ['id' => $id], false);
    }

    public function taskLogAddRecord($record) {
        $path = 'erp-task/record/add';
        return $this->put($path, $record, null, false);
    }

    public function taskLogUpdRecord($record) {
        $path = 'erp-task/record/upd';
        return $this->put($path, $record, null, false);
    }

    public function taskLogFindRecord($params) {
        $path = 'erp-task/record/find';
        return $this->get($path, $params, false);
    }

    public function taskLogAddLog($log) {
        $path = 'erp-task/log/add';
        return $this->put($path, $log, null, false);
    }

    public function taskLogBatchAddLog($logs) {
        $path = 'erp-task/log/add-batch';
        return $this->put($path, $logs, null, false);
    }

    public function taskLogFind($task_record_id) {
        $path = 'erp-task/log/find';
        return $this->get($path, ['task_record_id' => $task_record_id], false);
    }

    public function visitLogFind($mobile, $min_time, $cnt) {
        $path = 'visit-log/find';
        return $this->get($path, ['protyle_mobile' => $mobile, 'visit_time' => "{$min_time}000", 'pagesize' => $cnt], false);
    }

    /**
     * @param string|array $params
     */
    public function visitLogAdd($params) {
        $path = 'visit-log/add';
        $params = is_string($params) ? json_decode($params, true) : $params;
        return $this->put($path, $params, null, false);
    }

    /**
     * @param string|array $params
     */
    public function visitLogUpdateTime($params) {
        $path = 'visit-log/upd-time';
        $params = is_string($params) ? json_decode($params, true) : $params;
        return $this->put($path, $params, null, false);
    }

    /**
     * 添加流程日志
     *
     * @param string  $svc_flow_instance_id 实例ID
     * @param string  $svc_flow_node_name   当前执行节点名称
     * @param string  $log_content          日志内容
     * @param integer $log_order            日志顺序
     * @param string  $log_type             日志类型  info warn error
     *
     * @return \GuzzleHttp\Promise\PromiseInterface|mixed
     */
    public function svcFlowLogAdd($svc_flow_instance_id, $svc_flow_node_name, $log_content, $log_order, $log_type = 'info') {
        $path = 'svc-flow/log/add';
        return $this->put($path, ['svc_flow_instance_id' => $svc_flow_instance_id, 'node_name' => $svc_flow_node_name, 'log_type' => $log_type, 'log_msg' => $log_content, 'log_order' => $log_order, 'created_at' => time()], null, false);
    }

    /**
     * 查询流程日志
     *
     * @param $svc_flow_instance_id 实例ID
     *
     * @return \GuzzleHttp\Promise\PromiseInterface|mixed
     */
    public function svcFlowLogFind($svc_flow_instance_id) {
        $path = 'svc-flow/log/find';
        return $this->get($path, ['svc_flow_instance_id' => $svc_flow_instance_id], false);
    }

    /**
     * @param string $agent_id
     */
    public function fsAgentScoreFindByAgentId($agent_id) {
        $path = 'fs-agent-score/find-by-agentid';
        return $this->get($path, ['agent_id' => $agent_id], false);
    }

    /**
     * @param string|array $params
     */
    public function fsAgentScoreUpsert($params) {
        $path = 'fs-agent-score/upsert';
        $params = is_string($params) ? json_decode($params, true) : $params;
        return $this->put($path, $params, null, false);
    }

    public function fsRecommendFindByDicId($dic_id) {
        $path = 'fs-recommend/find-by-dicid';
        return $this->get($path, ['dic_id' => $dic_id], false);
    }

    /**
     * @param int $house_type
     */
    public function fsRecommendFindByHouseType($house_type) {
        $path = 'fs-recommend/find-by-housetype';
        return $this->get($path, ['housetype_id' => $house_type], false);
    }

    public function fsRecommendTruncate() {
        $path = 'fs-recommend/truncate';
        return $this->post($path, [], null, false);
    }

    /**
     * @param string|array $params
     */
    public function fsRecommendUpsert($params) {
        $path = 'fs-recommend/upsert';
        $params = is_string($params) ? json_decode($params, true) : $params;
        return $this->put($path, $params, null, false);
    }

    public function phoneBrowseFind($params) {
        $path = 'phone-browse/find';
        return $this->get($path, $params, false);
    }

    /**
     * @param string|array $params
     */
    public function phoneBrowseAdd($params) {
        $path = 'phone-browse/add';
        $params = is_string($params) ? json_decode($params, true) : $params;
        return $this->put($path, $params, false);
    }

}
