<?php

namespace Fstar\Client\Mongo;

use Fstar\Client\LogService;
use GuzzleHttp\Client;

class MongoHelper {
    private $base_url = null;
    private $client = null;
    private $log = null;

    public function __construct($config = []) {
        $config = empty($config) ? config('fstar-client') : $config;
        $this->base_url = data_get($config, 'mongodb_svc_url');
        $this->client = new Client();
        $this->log = new LogService('mongo', data_get($config, 'mongodb_svc_write_log', false));
    }

    protected function get($path, $query_params, $async = true) {
        return $this->request($path, $query_params, null, 'GET', $async);
    }

    protected function post($path, $body, $query_params = null, $async = true) {
        return $this->request($path, $query_params, $body, 'POST', $async);
    }

    protected function put($path, $body, $query_params = null, $async = true) {
        return $this->request($path, $query_params, $body, 'PUT', $async);
    }

    protected function request($path, $query_params, $body, $method = 'POST', $async = true) {
        $url = "{$this->base_url}{$path}";
        $params = [
            'timeout'         => 5,
            'connect_timeout' => 1,
            'verify'          => false
        ];
        if(!empty($query_params)) {
            $params['query'] = $query_params;
        }
        if(!empty($body)) {
            $params['json'] = $body;
        }
        $param_str = json_encode($params);
        $this->log->info("Req url:{$url} params:{$param_str}");
        if($async) {
            $response = $this->client->requestAsync($method, $url, $params)->wait();
        } else {
            $response = $this->client->request($method, $url, $params);
        }
        $content = $response->getBody()->getContents();
        $this->log->info("Resp body:{$content}");
        $data = json_decode($content, true);
        if($data['result'] != MongoConstants::SUCCESS) {
            throw new MongoException($data['msg']);
        }
        return $data['data'];
    }
}
