<?php

namespace Fstar\Client\Msgc;

class MsgcService extends MsgcHelper {
    function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * 发送短信
     *
     * @param string|array $receiver    短信接收号码，多个号码逗号分割或json数组
     * @param string|array $message     短信内容
     * @param string       $sms_app_key 短信应用KEY
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendSms($receiver, $message, $sms_app_key = MsgcConstants::ZHOME_SMS_DEFAULT) {
        $now      = time();
        $path     = '/api/sms/send';
        $message  = is_array($message) ? json_encode($message) : $message;
        $receiver = is_array($receiver) ? json_encode($receiver) : "{$receiver}";
        $data     = ["sms_content" => $message, "sms_receiver_mobile" => $receiver, "sms_app_key" => $sms_app_key];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业纯文本消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $content
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatText($app_key, $receiver_type, $content, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-text';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "content" => $content];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业纯文本卡片消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $title
     * @param $content
     * @param $open_url
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatTextCard($app_key, $receiver_type, $title, $content, $open_url, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-textcard';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "title" => $title, "content" => $content, "open_url" => $open_url];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业图文消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $title
     * @param $content
     * @param $open_url
     * @param $img_url
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatNews($app_key, $receiver_type, $title, $content, $open_url, $img_url, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-news';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "title" => $title, "content" => $content, "open_url" => $open_url,
                     "img_url"       => $img_url];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业Markdown格式消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $content
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatMarkdown($app_key, $receiver_type, $content, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-markdown';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "content" => $content];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业图片消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $media_url
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatImage($app_key, $receiver_type, $media_url, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-image';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "media_url" => $media_url];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业语音消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $media_url
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatVoice($app_key, $receiver_type, $media_url, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-voice';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "media_url" => $media_url];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业视频消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $media_url
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatVideo($app_key, $receiver_type, $media_url, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-video';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "media_url" => $media_url];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业文件消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $media_url
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatFile($app_key, $receiver_type, $media_url, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-file';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "media_url" => $media_url];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * 微信企业自定义消息推送
     *
     * @param $app_key
     * @param $receiver_type
     * @param $custom_content
     * @param $receiver
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendWechatCustom($app_key, $receiver_type, $custom_content, $receiver = "") {
        $now      = time();
        $path     = '/api/wechat/send-custom';
        $data     = ["receiver_type" => $receiver_type, "receiver" => $this->parseReceiver($receiver, $receiver_type), "app_key" => $app_key, "content" => $custom_content];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    public function getOpenIdByEmpId($emp_id, $app_key) {
        $now      = time();
        $path     = '/api/wechat/get-openid';
        $data     = ["app_key" => $app_key, "user_id" => "{$emp_id}"];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * IM 发送文本消息
     *
     * @param $receiver      string|array 接收人 用户ID
     * @param $content       string 消息内容
     * @param $sender        string 发送人 用户ID
     * @param $receiver_type integer MsgcHelper::IM_RECEIVER_TYPE_EMP
     * @param $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendImTextMsg($receiver, $content, $sender = null, $sync_other_machine = null, $receiver_type = null, $im_app_key = null, $extra_params = []) {
        $now      = time();
        $path     = '/api/im-msg/send-text';
        $receiver = is_array($receiver) ? json_encode($receiver) : $receiver;
        $data     = [
            'app_key'            => $this->getImAppKey($im_app_key),
            'sender'             => $sender,
            'receiver'           => $receiver,
            'content'            => $content,
            'sync_other_machine' => $sync_other_machine,
        ];
        $data     = $this->addImPubData(array_merge($data, $extra_params), $receiver_type);
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * IM 发送图片消息
     *
     * @param $receiver
     * @param $img_url
     * @param $img_width
     * @param $img_height
     * @param $sender
     * @param $receiver_type
     * @param $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendImImageMsg($receiver, $img_url, $img_width, $img_height, $sender = null, $sync_other_machine = null, $receiver_type = null, $im_app_key = null, $extra_params = []) {
        $now      = time();
        $path     = '/api/im-msg/send-image';
        $receiver = is_array($receiver) ? json_encode($receiver) : $receiver;
        $data     = [
            'app_key'            => $this->getImAppKey($im_app_key),
            'sender'             => $sender,
            'receiver'           => $receiver,
            'file_url'           => $img_url,
            'file_width'         => $img_width,
            'file_height'        => $img_height,
            'sync_other_machine' => $sync_other_machine,
        ];
        $data     = $this->addImPubData(array_merge($data, $extra_params), $receiver_type);
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * IM 发送文件消息
     *
     * @param $receiver
     * @param $file_url
     * @param $file_name
     * @param $sender
     * @param $receiver_type
     * @param $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendImFileMsg($receiver, $file_url, $file_name, $sender = null, $sync_other_machine = null, $receiver_type = null, $im_app_key = null, $extra_params = []) {
        $now       = time();
        $path      = '/api/im-msg/send-file';
        $receiver  = is_array($receiver) ? json_encode($receiver) : $receiver;
        try {
            $ret       = get_headers($file_url, true);
            $file_size = data_get($ret, 'Content-Length');
        }catch(\Exception $ex){
            $file_size = 0;
            $this->log->warn($ex->getMessage());
        }
        $data      = [
            'app_key'            => $this->getImAppKey($im_app_key),
            'sender'             => $sender,
            'receiver'           => $receiver,
            'file_url'           => $file_url,
            'file_name'          => $file_name,
            'file_size'          => $file_size,
            'sync_other_machine' => $sync_other_machine,
        ];
        $data      = $this->addImPubData(array_merge($data, $extra_params), $receiver_type);
        $data_str  = json_encode($data);
        $headers   = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * IM 发送视频消息
     *
     * @param $receiver
     * @param $img_url
     * @param $img_width
     * @param $img_height
     * @param $sender
     * @param $receiver_type
     * @param $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendImVideoMsg($receiver, $video_url, $sender = null, $sync_other_machine = null, $receiver_type = null, $im_app_key = null, $extra_params = []) {
        $now      = time();
        $path     = '/api/im-msg/send-video';
        $receiver = is_array($receiver) ? json_encode($receiver) : $receiver;
        $data     = [
            'app_key'            => $this->getImAppKey($im_app_key),
            'sender'             => $sender,
            'receiver'           => $receiver,
            'file_url'           => $video_url,
            'sync_other_machine' => $sync_other_machine,
        ];
        $data     = $this->addImPubData(array_merge($data, $extra_params), $receiver_type);
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * IM 发送语音文件
     *
     * @param $receiver
     * @param $audio_url
     * @param $audio_second
     * @param $sender
     * @param $receiver_type
     * @param $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendImAudioMsg($receiver, $audio_url, $audio_second, $sender = null, $sync_other_machine = null, $receiver_type = null, $im_app_key = null, $extra_params = []) {
        $now       = time();
        $path      = '/api/im-msg/send-sound';
        $ret       = get_headers($audio_url, true);
        $file_size = data_get($ret, 'Content-Length');
        $receiver  = is_array($receiver) ? json_encode($receiver) : $receiver;
        $data      = [
            'app_key'            => $this->getImAppKey($im_app_key),
            'sender'             => $sender,
            'receiver'           => $receiver,
            'file_url'           => $audio_url,
            'file_size'          => $file_size,
            'file_second'        => $audio_second,
            'sync_other_machine' => $sync_other_machine,
        ];
        $data      = $this->addImPubData(array_merge($data, $extra_params), $receiver_type);
        $data_str  = json_encode($data);
        $headers   = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * IM 发送位置信息
     *
     * @param $receiver
     * @param $content
     * @param $lat
     * @param $lng
     * @param $sender
     * @param $receiver_type
     * @param $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendImLocationMsg($receiver, $content, $lat, $lng, $sender = null, $sync_other_machine = null, $receiver_type = null, $im_app_key = null, $extra_params = []) {
        $now      = time();
        $path     = '/api/im-msg/send-location';
        $receiver = is_array($receiver) ? json_encode($receiver) : $receiver;
        $data     = [
            'app_key'            => $this->getImAppKey($im_app_key),
            'sender'             => $sender,
            'receiver'           => $receiver,
            'content'            => $content,
            'sync_other_machine' => $sync_other_machine,
            'msg_ext_info'       => json_encode(['lat' => $lat, 'lng' => $lng]),
        ];
        $data     = $this->addImPubData(array_merge($data, $extra_params), $receiver_type);
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * IM 发送自定义消息
     *
     * @param $receiver
     * @param $title
     * @param $content
     * @param $data
     * @param $sender
     * @param $ext_info
     * @param $receiver_type
     * @param $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function sendImCustomMsg($receiver, $title, $content, $data, $sender = null, $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null, $extra_params = []) {
        $now      = time();
        $path     = '/api/im-msg/send-custom';
        $receiver = is_array($receiver) ? json_encode($receiver) : $receiver;
        $data     = [
            'app_key'            => $this->getImAppKey($im_app_key),
            'sender'             => $sender,
            'receiver'           => $receiver,
            'title'              => $title,
            'content'            => $content,
            'data'               => is_array($data) ? json_encode($data) : $data,
            'sync_other_machine' => $sync_other_machine,
            'msg_ext_info'       => is_string($ext_info) ? $ext_info : json_encode($ext_info),
        ];
        $data     = $this->addImPubData(array_merge($data, $extra_params), $receiver_type);
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    public function sendImZhomeH5Msg($receiver, $title, $content, $open_url, $sender, $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null) {
        $im_data = [
            'msg_type'    => 'h5',
            'h5_msg_data' => [
                'title'   => $title,
                'content' => $content,
                'link'    => $open_url
            ]
        ];
        return $this->sendImCustomMsg($receiver, $title, $content, $im_data, $sender, $sync_other_machine, $ext_info, $receiver_type, $im_app_key);
    }

    public function sendImZhomeUniappMsg($receiver, $title, $content, $node_key, $open_url, $sender = null, $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null) {
        if(strpos($open_url, 'v2/') === 0 || strpos($open_url, 'v2%2F') === 0) {
            $open_url = strpos($open_url, 'v2/') ? urlencode($open_url) : $open_url;
            $open_url = "pages/index/index?path={$open_url}";
        }
        $im_data = [
            'msg_type'         => 'uni_app',
            'uni_app_msg_data' => [
                "title"     => $title,
                "content"   => $content,
                "node_key"  => $node_key,
                "open_page" => $open_url,
            ]
        ];
        return $this->sendImCustomMsg($receiver, $title, $content, $im_data, $sender, $sync_other_machine, $ext_info, $receiver_type, $im_app_key);
    }

    public function sendImZhomeCustMsg($receiver, $title, $content, $cust_id, $extra_params = [], $sender = null, $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null) {
        $im_data = [
            'msg_type'          => 'cust_tip',
            'cust_tip_msg_data' => array_merge(['title' => $title, 'content' => $content, 'ct_id' => $cust_id, 'img_url' => '', 'ext_data' => ['type' => 'zhome_cust']], $extra_params)
        ];
        return $this->sendImCustomMsg($receiver, $title, $content, $im_data, $sender, $sync_other_machine, $ext_info, $receiver_type, $im_app_key);
    }

    public function sendImZhomeHsSaleMsg($receiver, $title, $content, $hs_id, $extra_params = [], $sender = null, $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null) {
        $im_data = [
            'msg_type'             => 'hs_sale_tip',
            'hs_sale_tip_msg_data' => array_merge(['title' => $title, 'content' => $content, 'house_id' => $hs_id, 'img_url' => '', 'ext_data' => ['type' => 'zhome_hs_sale']], $extra_params)
        ];
        return $this->sendImCustomMsg($receiver, $title, $content, $im_data, $sender, $sync_other_machine, $ext_info, $receiver_type, $im_app_key);
    }

    public function sendImZhomeHsRentMsg($receiver, $title, $content, $hs_id, $extra_params = [], $sender = null, $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null) {
        $im_data = [
            'msg_type'             => 'hs_rent_tip',
            'hs_rent_tip_msg_data' => array_merge(['title' => $title, 'content' => $content, 'house_id' => $hs_id, 'img_url' => '', 'ext_data' => ['type' => 'zhome_hs_rent']], $extra_params)
        ];
        return $this->sendImCustomMsg($receiver, $title, $content, $im_data, $sender, $sync_other_machine, $ext_info, $receiver_type, $im_app_key);
    }

    public function callImApi($api, $params, $im_app_key = null) {
        return $this->imCall($api, $params, $im_app_key);
    }

    private function parseReceiver($receiver, $receiver_type = MsgcConstants::RECEIVER_TYPE_EMP) {
        if($receiver_type == MsgcConstants::RECEIVER_TYPE_ALL) {
            return '[]';
        }
        if(empty($receiver)) {
            throw new MsgcException("消息接收人不能为空");
        }
        if(is_array($receiver)) {
            return json_encode($receiver);
        }
        return $receiver;
    }

}
