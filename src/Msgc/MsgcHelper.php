<?php

namespace Fstar\Client\Msgc;

use Fstar\Client\FsFinance\FsFinanceException;
use Fstar\Client\LogService;
use GuzzleHttp\Client;

class MsgcHelper {
    private $msgc_user_no;
    private $msgc_token;
    private $msgc_host;
    private $im_app_key;
    protected $log;
    protected $im_app_cate;

    function __construct($config) {
        $this->msgc_host = data_get($config, 'msgc_host');
        $this->msgc_token = data_get($config, 'msgc_token');
        $this->msgc_user_no = data_get($config, 'msgc_user_no');
        $this->im_app_key = data_get($config, 'msgc_im_app_key');
        $this->im_app_cate = data_get($config, 'msgc_im_app_cate', 'fangstar');
        $this->log = new LogService('msgc', data_get($config, 'msgc_write_log', true));
    }

    protected function getHeaders($now, $data) {
        $auth = $this->msgc_user_no.';'.md5($now.$this->msgc_token.$data);
        return ['Content-Type' => 'application/json', 'Authorization' => $auth, 'Date' => $now];
    }

    protected function post($headers, $path, $data, $ret_data = false) {
        $url = "{$this->msgc_host}{$path}";
        $param_str = is_array($data) ? json_encode($data) : $data;
        $header_str = is_array($headers) ? json_encode($headers) : $headers;
        $this->log->info("Req url:{$url} headers:{$header_str} params:{$param_str}");
        try {
            $response = (new Client())->request('POST', $url, [
                'headers'         => $headers,
                'timeout'         => 60,
                'connect_timeout' => 5,
                'verify'          => false,
                'body'            => $data
            ]);
            $content = $response->getBody()->getContents();
            $this->log->info("Resp body:{$content}");
            if(!is_array($content)) {
                $content = json_decode($content, true);
            }
            if($ret_data) {
                return data_get($content, 'data');
            }
            return $content;
        } catch(\Exception $ex) {
            throw new MsgcException("消息中心接口请求失败，{$ex->getMessage()}");
        }
    }

    protected function addImPubData($data, $receiver_type, $receiver_id_cate = null, $sender_id_cate = null) {
        return array_merge($data, [
            'receiver_type'    => empty($receiver_type) ? MsgcConstants::IM_RECEIVER_TYPE_EMP : $receiver_type,
            'receiver_id_cate' => empty($receiver_id_cate) ? MsgcConstants::IM_USER_ID_CATE_USER : $receiver_id_cate,
            'sender_id_type'   => empty($sender_id_cate) ? MsgcConstants::IM_USER_ID_CATE_USER : $sender_id_cate
        ]);
    }

    public function imCall($path, $content, $app_key = null) {
        $req_path = '/api/im/call';
        $data = [
            'path'    => $path,
            'content' => $content,
            'app_key' => $this->getImAppKey($app_key)
        ];
        $now = time();
        $data_str = json_encode($data);
        $headers = $this->getHeaders($now, $data_str);
        $ret_data_str = $this->post($headers, $req_path, $data_str, true);
        return json_decode($ret_data_str, true);
    }

    protected function getImAppKey($app_key) {
        return empty($app_key) ? $this->im_app_key : $app_key;
    }
}