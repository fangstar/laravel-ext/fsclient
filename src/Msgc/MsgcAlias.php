<?php

namespace Fstar\Client\Msgc;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * @see \Fstar\Client\Msgc\MsgcService
 *
 * @method static MsgcService newInstanse()
 * @method static sendSms(string|array $receiver, string|array $message, string $sms_app_key = MsgcConstants::ZHOME_SMS_DEFAULT)
 * @method static sendWechatText(string $app_key, string $receiver_type, string $content, string $receiver = "")
 * @method static sendWechatTextCard(string $app_key, string $receiver_type, string $title, string $content, string $open_url, string $receiver = "")
 * @method static sendWechatNews(string $app_key, string $receiver_type, string $title, string $content, string $open_url, string $img_url, string $receiver = "")
 * @method static sendWechatMarkdown(string $app_key, string $receiver_type, string $content, string $receiver = "")
 * @method static sendWechatImage(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
 * @method static sendWechatVoice(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
 * @method static sendWechatVideo(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
 * @method static sendWechatFile(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
 * @method static sendWechatCustom(string $app_key, string $receiver_type, string $custom_content, string $receiver = "")
 * @method static getOpenIdByEmpId(string|integer $emp_id, string $app_key = MsgcConstants::ZHOME_PHONE_BOOK)
 * @method static sendImTextMsg(string|integer|array $receiver, string $content, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
 * @method static sendImImageMsg(string|integer|array $receiver, string $img_url, integer $img_width, integer $img_height, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
 * @method static sendImFileMsg(string|integer|array $receiver, string $file_url, string $file_name, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
 * @method static sendImVideoMsg(string|integer|array $receiver, string $video_url, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
 * @method static sendImAudioMsg(string|integer|array $receiver, string $audio_url, integer $audio_second, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
 * @method static sendImLocationMsg(string|integer|array $receiver, string $content, float $lat, float $lng, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
 * @method static sendImCustomMsg(string|integer|array $receiver, string $title, string $content, array|string $data, string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
 * @method static sendImZhomeH5Msg(string|integer|array $receiver, string $title, string $content, string $open_url, string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
 * @method static sendImZhomeUniappMsg(string|integer|array $receiver, string $title, string $content, string $node_key, string $open_url, string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
 * @method static sendImZhomeCustMsg(string|integer|array $receiver, string $title, string $content, string|integer $cust_id, array $extra_params = [], string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
 * @method static sendImZhomeHsSaleMsg(string|integer|array $receiver, string $title, string $content, string $hs_id, array $extra_params = [], string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
 * @method static sendImZhomeHsRentMsg(string|integer|array $receiver, string $title, string $content, string $hs_id, array $extra_params = [], string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
 */
class MsgcAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_msgc;
    }
}