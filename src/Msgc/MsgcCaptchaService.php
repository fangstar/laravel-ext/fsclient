<?php

namespace Fstar\Client\Msgc;

class MsgcCaptchaService extends MsgcHelper {
    function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * @param string $type      验证码类型
     * @param string $img_group 图片分组
     *
     * @return array|mixed|string
     * @throws MsgcException
     */
    public function generate($type = MsgcConstants::CAPTCHA_TYPE_SLIDER, $img_group = null) {
        $now      = time();
        $path     = '/api/captcha/generate';
        $data     = ["type" => $type, "img_group" => $img_group];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * @param string  $id                     验证码ID
     * @param array   $captcha_track          验证数据
     * @param boolean $open_two_step_valid    是否启用二次验证
     * @param integer $two_step_valid_overdue 二次验证超时时间(秒)
     *
     * @return array|mixed|string
     * @throws MsgcException
     */
    public function matching($id, $captcha_track, $open_two_step_valid = false, $two_step_valid_overdue = 300) {
        $now      = time();
        $path     = '/api/captcha/matching';
        $data     = ["id" => $id, "data" => $captcha_track, 'open_two_step_valid' => $open_two_step_valid, 'two_step_valid_overdue' => $two_step_valid_overdue];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }

    /**
     * @param string $token matching方法返回的token
     *
     * @return array|mixed|string
     * @throws MsgcException
     */
    public function tokenValid($token) {
        $now      = time();
        $path     = '/api/captcha/is-valid';
        $data     = ["id" => $token];
        $data_str = json_encode($data);
        $headers  = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str);
    }
}
