<?php

namespace Fstar\Client\Msgc\Im;

use Fstar\Client\Constants;
use Fstar\Client\Msgc\MsgcConstants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\Msgc\Im\MsgcImUserService
 *
 * @method static MsgcImUserService newInstanse()
 * @method static login($user_id, $user_name, $avatar = '', $gender = MsgcConstants::IM_USER_GENDER_UNKNOW, $im_app_key = null)
 * @method static getUserByUserIds(array $user_ids, string $im_app_key = null)
 * @method static getUserByUserId(string|integer $user_id, string $im_app_key = null)
 * @method static getUserByImIds(string $im_ids, string $im_app_key = null)
 * @method static getUserByImId(string $im_id, string $im_app_key = null)
 * @method static createGroup(array $content)
 * @method static modifyGroupBaseInfo(array $content)
 * @method static destroyGroup(mixed $group_id)
 * @method static importGroupMember(array $content)
 * @method static removeGroupMember(mixed $group_id, array $im_ids, integer $silence = 0)
 * @method static modifyGroupMemberInfo(array $content)
 */
class MsgcImUserAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_msgc_im;
    }
}