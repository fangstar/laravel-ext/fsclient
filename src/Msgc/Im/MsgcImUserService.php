<?php

namespace Fstar\Client\Msgc\Im;

use Fstar\Client\Msgc\MsgcConstants;
use Fstar\Client\Msgc\MsgcHelper;

class MsgcImUserService extends MsgcHelper {

    function __construct($config) {
        parent::__construct($config);
    }

    public function login($user_id, $user_name, $avatar = '', $gender = MsgcConstants::IM_USER_GENDER_UNKNOW, $im_app_key = null) {
        $now = time();
        $path = '/api/im-user/login';
        $user_info = [
            'app_key'       => $this->getImAppKey($im_app_key),
            'user_id'       => "{$user_id}",
            'user_name'     => $user_name,
            'user_face_url' => $avatar,
            'user_gender'   => $gender,
            'user_app_cate' => $this->im_app_cate,
        ];
        $data_str = json_encode($user_info);
        $headers = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str, true);
    }

    /**
     * @param array $user
     *  [
     *  "user_id":"update required",
     *  "user_no":"",
     *  "user_name":"",
     *  "user_face_url":"",
     *  "user_gender":"",
     *  "user_birthday":"",
     *  "user_location":"",
     *  "user_self_signature":"",
     *  "user_add_allow_type":"",
     *  "user_msg_set":"",
     *  "user_level":"",
     *  "user_level_name":"",
     *  "user_role":"",
     *  "user_role_name":"",
     *  "user_tag":"",
     *  "user_src":"",
     *  ]
     * @param       $im_app_key
     *
     * @return array|mixed|string
     * @throws \Fstar\Client\FsFinance\FsFinanceException
     */
    public function setUserInfo(array $user, $im_app_key = null) {
        $now = time();
        $path = '/api/im-user/set-user-info';
        $user = array_merge($user, [
            'app_key'       => $this->getImAppKey($im_app_key),
            'user_app_cate' => $this->im_app_cate,
        ]);
        $data_str = json_encode($user);
        $headers = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str, true);
    }

    public function getUserByUserIds($user_ids, $im_app_key = null) {
        $user_ids = is_array($user_ids) ? $user_ids : [$user_ids];
        foreach($user_ids as $idx => $user_id) {
            $user_ids[$idx] = "{$user_id}";
        }
        $now = time();
        $path = '/api/im-user/get-im-ids';
        $data_str = json_encode(['user_ids' => $user_ids, 'app_key' => $this->getImAppKey($im_app_key)]);
        $headers = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str, true);
    }

    public function getUserByUserId($user_id, $im_app_key = null) {
        $im_list = $this->getUserByUserIds([$user_id], $this->getImAppKey($im_app_key));
        if(!empty($im_list) && is_array($im_list)) {
            return $im_list[0];
        } else {
            return [];
        }
    }

    public function getUserByImIds($im_ids, $im_app_key = null) {
        $im_ids = is_array($im_ids) ? $im_ids : [$im_ids];
        foreach($im_ids as $idx => $im_id) {
            $im_ids[$idx] = "{$im_id}";
        }
        $now = time();
        $path = '/api/im-user/get-user-by-im-ids';
        $data_str = json_encode(['im_ids' => $im_ids, 'app_key' => $this->getImAppKey($im_app_key)]);
        $headers = $this->getHeaders($now, $data_str);
        return $this->post($headers, $path, $data_str, true);
    }

    public function getUserByImId($im_id, $im_app_key = null) {
        $user_list = $this->getUserByImIds([$im_id], $this->getImAppKey($im_app_key));
        if(!empty($user_list) && is_array($user_list)) {
            return $user_list[0];
        } else {
            return [];
        }
    }


    /**
     * 创建分组 https://cloud.tencent.com/document/product/269/1615
     *
     * @param $content
     *
     * @return array
     */
    public function createGroup($content) {
        $path = '/v4/group_open_http_svc/create_group';
        return $this->imCall($path, $content);
    }

    /**
     * 修改组基本信息 https://cloud.tencent.com/document/product/269/1620
     *
     * @param $content
     *
     * @return array
     */
    public function modifyGroupBaseInfo($content) {
        $path = '/v4/group_open_http_svc/modify_group_base_info';
        return $this->imCall($path, $content);
    }

    /**
     * 销毁分组 https://cloud.tencent.com/document/product/269/1624
     *
     * @param $group_id
     *
     * @return array
     */
    public function destroyGroup($group_id) {
        $path = '/v4/group_open_http_svc/destroy_group';
        $content = ['GroupId' => $group_id];
        return $this->imCall($path, $content);
    }

    /**
     * 导入组成员 https://cloud.tencent.com/document/product/269/1636
     *
     * @param $content
     *
     * @return array
     */
    public function importGroupMember($content) {
        $path = '/v4/group_open_http_svc/import_group_member';
        return $this->imCall($path, $content);
    }

    /**
     * 移除群成员 https://cloud.tencent.com/document/product/269/1622
     *
     * @param string $group_id
     * @param array  $im_ids
     * @param        $silence
     *
     * @return array
     */
    public function removeGroupMember(string $group_id, array $im_ids, $silence = 0) {
        $path = '/v4/group_open_http_svc/delete_group_member';
        $content = ['GroupId' => $group_id, 'Silence' => $silence, 'MemberToDel_Account' => $im_ids];
        return $this->imCall($path, $content);
    }

    /**
     * 修改群成员信息 https://cloud.tencent.com/document/product/269/1623
     *
     * @param $content
     *
     * @return array
     */
    public function modifyGroupMemberInfo($content) {
        $path = '/v4/group_open_http_svc/modify_group_member_info';
        return $this->imCall($path, $content);
    }
}