<?php

namespace Fstar\Client\Msgc;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\Msgc\MsgcCaptchaService
 *
 * @method static MsgcCaptchaService newInstanse()
 * @method static generate(string $type = MsgcConstants::CAPTCHA_TYPE_SLIDER, string $img_group = null)
 * @method static matching(string $id, array $captcha_track, boolean $open_two_step_valid = false, int $two_step_valid_overdue = 300)
 * @method static tokenValid(string $token)
 */
class MsgcCaptchaAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_msgc_captcha;
    }
}