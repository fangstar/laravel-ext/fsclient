<?php

namespace Fstar\Client\Msgc;

class MsgcConstants {
    const RET_SUCCESS = 'success';
    const RET_ERROR = 'error';
    
    const IM_USER_ID_CATE_IM = 'im_id';
    const IM_USER_ID_CATE_USER = 'user_id';
    //
    const IM_RECEIVER_TYPE_ALL = 1;
    const IM_RECEIVER_TYPE_EMP = 2;
    const IM_RECEIVER_TYPE_ATTR = 3;
    const IM_RECEIVER_TYPE_TAG = 4;
    //
    const IM_USER_GENDER_UNKNOW = 'Gender_Type_Unknown';
    const IM_USER_GENDER_MALE = 'Gender_Type_Male';
    const IM_USER_GENDER_FEMALE = 'Gender_Type_Female';

    const IM_SYNC_OTHER_MACHINE_YES = 'yes';
    //
    const IM_SENDER_NETWORK_PORT = 'sys_network_port';
    const IM_SENDER_RUN_DIC_AUDIT = 'sys_run_dic_audit';
    const IM_SENDER_REFUND_CONFIRM = 'sys_refund_confirm';
    const IM_SENDER_WORK_ORDER = 'sys_work_order';
    const IM_SENDER_REAL_HS_AUDIT = 'sys_real_hs_audit';
    const IM_SENDER_ATTEN = 'sys_atten';
    const IM_SENDER_CONTACT = 'sys_contact';
    const IM_SENDER_RECEIPT = 'sys_receipt';
    const IM_SENDER_EXAM_ONLINE = 'sys_exam_online';
    const IM_SENDER_AUDIT_CHANGE_PHONE = 'sys_audit_change_phone';
    const IM_SENDER_MAIN_HS_ORDER = 'sys_main_hs_order';
    const IM_SENDER_HS_STATISTICS = 'sys_hs_statistics';
    const IM_SENDER_COM_CHECK = 'sys_com_check';
    const IM_SENDER_SALARY_CHECK = 'sys_salary_check';
    const IM_SENDER_NHS_STATISTICS = 'sys_nhs_statistics';
    const IM_SENDER_RUN_DIC_REC = 'sys_run_dic_rec';
    const IM_SENDER_TRAIN = 'sys_train';
    const IM_SENDER_HS_KNOWLEDGE_LIB = 'sys_hs_knowledge_lib';
    const IM_SENDER_CHANGE_OWNER_HELPER = 'sys_change_owner_helper';
    const IM_SENDER_MRD = 'sys_mrd';
    const IM_SENDER_REFUND_AUDIT = 'sys_refund_audit';
    const IM_SENDER_AGENT = 'sys_agent';
    const IM_SENDER_RP_HEPLER = 'sys_rp_hepler';
    const IM_SENDER_NHS_SELL_DOC = 'sys_nhs_sell_doc';
    const IM_SENDER_HS_CHANGE_CHARGER = 'sys_hs_change_charger';
    const IM_SENDER_NHS_PASS = 'sys_nhs_pass';
    const IM_SENDER_ERR_LISTENER = 'sys_err_listener';
    const IM_SENDER_SALE_HS = 'sys_sale_hs';
    const IM_SENDER_HS_KEY = 'sys_hs_key';
    const IM_SENDER_ANNOUNCEMENT = 'sys_announcement';
    const IM_SENDER_NHS_REPORT = 'sys_nhs_report';
    const IM_SENDER_ZHOME_TABLOID = 'sys_zhome_tabloid';
    const IM_SENDER_ZHOME_HS_PRICE_CHANGE = 'sys_hs_price_change';
    const IM_SENDER_ZHOME_DOWNLOAD = 'sys_download';
    //
    const RECEIVER_TYPE_ALL = 1;
    const RECEIVER_TYPE_EMP = 2;
    const RECEIVER_TYPE_DEPT = 3;
    const RECEIVER_TYPE_TAG = 4;
    //
    const ZHOME_SMS_DEFAULT = 'ERP_DEFAULT';
    const ZHOME_SMS_CHANGE_PWD = 'ERP_CHANGE_PWD';
    const ZHOME_SMS_CHANGE_DEVICE = 'ERP_CHANGE_DEVICE';
    const ZHOME_SMS_CHANGE_MOBILE = 'ERP_CHANGE_MOBILE';
    const ZHOME_NOTIFY_MOBILE_CHANGE_SUCC = 'ERP_NOTIFY_MOBILE_CHANGE_SUCC';
    const ZHOME_SMS_ERP_SH_SEEN_CONFIRM = 'ERP_SH_SEEN_CONFIRM';
    const ZHOME_SMS_ERP_ADD_MOBILE = 'ERP_ADD_MOBILE';

    const ERP_RENT_CONTRACT = "ERP_RENT_CONTRACT"; // 租房合同验证码
    const ERP_RENT_CONTRACT_VOICE = "ERP_RENT_CONTRACT_VOICE"; // 租房合同语言验证码
    const ERP_FAST_SELL_CONTRACT = "ERP_FAST_SELL_CONTRACT"; // 速销合同验证码
    const ERP_FAST_SELL_CONTRACT_VOICE = "ERP_FAST_SELL_CONTRACT_VOICE"; // 速销合同语言验证码
    const ERP_NOTIFY_LEAVE_HANDLING = "ERP_NOTIFY_LEAVE_HANDLING"; // 离职手续办理通知
    const ERP_NOTIFY_ADD_HANDLING = "ERP_NOTIFY_ADD_HANDLING"; // 新添加员工考勤使用通知
    const ERP_NOTIFY_ENTRY_HANDLING = "ERP_NOTIFY_ENTRY_HANDLING"; // 入职系统使用通知
    const ERP_HS_KEY_LEND_OUT_VERIFY = "ERP_HS_KEY_LEND_OUT_VERIFY"; // 钥匙外借验证
    const ERP_RUN_DIC_CODE_VERIFY = "ERP_RUN_DIC_CODE_VERIFY"; // 跑盘登记
    const COMMON_CODE_VERIFY_ZHOME = "COMMON_CODE_VERIFY_ZHOME"; // 通用验证码
    const COMMON_CODE_VERIFY_FS = "COMMON_CODE_VERIFY_FS"; // 通用验证码

    const FSCALL_TASK_FINISH_NOTIFY = "FSCALL_TASK_FINISH_NOTIFY"; // 任务通知

    //
    const ZHOME_WARNING_PUSH = "ZHOME_WARNING_PUSH";  //告警消息推送
    const ZHOME_CORP_HELPER = "ZHOME_CORP_HELPER";  //企业小助手
    const ZHOME_CHANGE_HS_CHANGER = "ZHOME_CHANGE_HS_CHANGER";  //划盘工单
    const ZHOME_REFUND_AUDIT = "ZHOME_REFUND_AUDIT";  //退款审批
    const ZHOME_SALARY_CHECK = "ZHOME_SALARY_CHECK";  //工资核对详情
    const ZHOME_CHANGE_PHONE_AUDIT = "ZHOME_CHANGE_PHONE_AUDIT";  //审核更换手机
    const ZHOME_RECEIPT = "ZHOME_RECEIPT";  //收据单
    const ZHOME_CONTRACT_MGR = "ZHOME_CONTRACT_MGR";  //合同管理
    const ZHOME_ATTEN_MGR = "ZHOME_ATTEN_MGR";  //考勤管理
    const ZHOME_WORK_ORDER_MGR = "ZHOME_WORK_ORDER_MGR";  //工单管理
    const ZHOME_STEREO_HS_AUDIT = "ZHOME_STEREO_HS_AUDIT";  //真房源审核
    const ZHOME_REFUND_AUDIT_CONFIRM = "ZHOME_REFUND_AUDIT_CONFIRM";  //退款审批确认
    const ZHOME_RUN_DIC_ORDER = "ZHOME_RUN_DIC_ORDER";  //处理跑盘工单
    const ZHOME_NETWORK_PORT = "ZHOME_NETWORK_PORT";  //网络端口
    const ZHOME_PHONE_BOOK = "ZHOME_PHONE_BOOK";  //网络端口
    const ZHOME_REAL_LOOK = "ZHOME_REAL_LOOK_HELPER";  //实勘助手  19

    const CAPTCHA_TYPE_SLIDER           = 'SLIDER';          //滑块
    const CAPTCHA_TYPE_ROTATE           = 'ROTATE';          //旋转
    const CAPTCHA_TYPE_CONCAT           = 'CONCAT';          //拼接
    const CAPTCHA_TYPE_WORD_IMAGE_CLICK = 'WORD_IMAGE_CLICK';//文字图片点选
}
