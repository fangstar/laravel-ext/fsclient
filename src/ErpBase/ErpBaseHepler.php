<?php

namespace Fstar\Client\ErpBase;

use Fstar\Client\Constants;
use Fstar\Client\LogService;
use GuzzleHttp\Client;

class ErpBaseHepler {

    private $oprater_id = null;
    private $base_url = null;
    private $log = null;

    function __construct($config = []) {
        $config = empty($config) ? config('fstar-client') : $config;
        $oprater_id = Constants::SYS_EMP_ID;
        $session_emp_id = session('emp_id', Constants::SYS_EMP_ID);
        $this->oprater_id = ($oprater_id == Constants::SYS_EMP_ID) ? $session_emp_id : $oprater_id;
        $this->base_url = data_get($config, "erp_base_url");
        $this->log = new LogService('erp_base', data_get($config, 'erp_base_write_log', true));
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * 调用ERP_BASE服务统一封装方法
     *
     * @param type string $path
     * @param type string|arrau $data 传递的数据
     * @param type array $params HTTP 请求参数
     *
     * @throws \Exception
     */
    protected function post($path, $data, $params = ['timeout' => 30]) {
        $uri = "{$this->base_url}{$path}";
        $req_data = $data;
        if(!is_string($data)) {
            $req_data = json_encode($data);
        }
        $this->log->info("Req url:{$uri} params:{$req_data}");
        $client = new Client();
        $response = $client->request('POST', $uri, [
            'form_params'     => ['data' => $req_data],
            'timeout'         => $params['timeout'],
            'connect_timeout' => 5,
            'verify'          => false
        ]);
        $contents = $response->getBody()->getContents();
        $this->log->info("Resp body:{$contents}");
        $arr = json_decode($contents, true);
        if($arr['result'] != 1) {
            throw new ErpBaseException($arr['msg']);
        }
        return $arr['data'];
    }
}
