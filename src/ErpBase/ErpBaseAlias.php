<?php

namespace Fstar\Client\ErpBase;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\ErpBase\ErpBaseService
 *
 * @method static post(string $path, string|array $data, array $params = ['timeout' => 30])
 */
class ErpBaseAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_erp_base;
    }
}