<?php

namespace Fstar\Client\EagleEye;

class EagleEyeConstants {
    const SUCCESS = 1;
    const ERROR = 0;

    const TYPE_IDCARD_FRONT = 'idcard-front';
    const TYPE_IDCARD_BACK = 'idcard-back';
    const TYPE_BANK_CARD = 'bankcard';
}