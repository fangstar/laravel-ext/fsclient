<?php

namespace Fstar\Client\EagleEye;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\EagleEye\EagleEyeService
 *
 * @method static EagleEyeService newInstanse()
 * @method static htmlToPdf(string $html_content, array $settings = [])
 * @method static htmlToImage(string $html_content, array $settings = [])
 * @method static urlToImage(string $html_content, array $settings = [])
 * @method static textOcr(string $image, string $type, array $settings = [])
 * @method static textOcrBankCard(string $image, array $settings = [])
 * @method static textOcrIdCardFront(string $image, array $settings = [])
 * @method static textOcrIdCardBack(string $image, array $settings = [])
 * @method static mobileRecognize(string $mobile, array $settings = [])
 * @method static coordinateBD09ToGCJ02(array $coordinates = [[lat,lon]], $order = 'latlon')
 * @method static coordinateBD09ToWGS84(array $coordinates = [[lat,lon]], $order = 'latlon')
 * @method static coordinateGCJ02ToWGS84(array $coordinates = [[lat,lon]], $order = 'latlon')
 * @method static coordinateGCJ02ToBD09(array $coordinates = [[lat,lon]], $order = 'latlon')
 * @method static coordinateWGS84ToGCJ02(array $coordinates = [[lat,lon]], $order = 'latlon')
 * @method static coordinateWGS84ToBD09(array $coordinates = [[lat,lon]], $order = 'latlon')
 */
class EagleEyeAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_eagleeye;
    }
}