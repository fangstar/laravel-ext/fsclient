<?php

namespace Fstar\Client\EagleEye;

use Fstar\Client\LogService;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;

class EagleEyeHelper {
    private $user_no = null;
    private $token = null;
    private $host = null;
    private $only_data = null;
    private $log = null;

    function __construct($config = []) {
        $config = empty($config) ? config('fstar-client') : $config;
        $this->host = data_get($config, 'eagle_eye_host');
        $this->token = data_get($config, 'eagle_eye_token');
        $this->user_no = data_get($config, 'eagle_eye_user_no');
        $this->only_data = data_get($config, 'eagle_eye_ret_data', 'false');
        $this->log = new LogService('eagle_eye', data_get($config, 'eagle_eye_write_log', true));
    }

    protected function post($path, $params) {
        $url = "{$this->host}{$path}";
        $data = is_array($params) ? json_encode($params) : $params;
        $headers = $this->getHeaders(time(), $data);
        $param_str = is_array($params) ? json_encode(Arr::except($params, ['image'])) : $params;
        $this->log->info("Req url:{$url} params:{$param_str}");
        try {
            $response = (new Client())->request('POST', $url, [
                'headers'         => $headers,
                'timeout'         => 60,
                'connect_timeout' => 5,
                'verify'          => false,
                'body'            => $data
            ]);
            $content = $response->getBody()->getContents();
            $this->log->info("Resp body:{$content}");
            if(!is_array($content)) {
                $content = json_decode($content, true);
            }
            if($this->only_data) {
                return array_get($content, 'data');
            }
            return $content;
        } catch(\Exception $ex) {
            throw new EagleEyeException("鹰眼系统接口请求失败", $ex->getCode(), $ex->getPrevious());
        }
    }

    protected function imgToBase64($img_url) {
        if(mb_strpos($img_url, 'data:') === 0) {
            return $img_url;
        }
        $img_info = getimagesize($img_url);
        return "data:{$img_info['mime']};base64,".chunk_split(base64_encode(file_get_contents($img_url)));
    }

    private function getHeaders($now, $data) {
        $auth = $this->user_no.';'.md5($now.$this->token.$data);
        return [
            'Content-Type'    => 'application/json',
            'Authorization'   => $auth,
            'Date'            => $now,
            'Accept-Language' => 'zh-CN'
        ];
    }
}
