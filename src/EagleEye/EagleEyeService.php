<?php

namespace Fstar\Client\EagleEye;

use Fstar\Client\LogService;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;

class EagleEyeService extends EagleEyeHelper {


    function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * html文本转换PDF settings 设置查看 https://wkhtmltopdf.org/usage/wkhtmltopdf.txt
     *
     * @param $html_content
     * @param $settings [
     *                  ["--header-html-str", ""],
     *                  ["--footer-html-str", ""],
     *                  ["--orientation", "Landscape"],
     *                  ["--page-size", "A4"]
     *                  ]
     *
     * @return mixed
     */
    public function htmlToPdf(string $html_content, array $settings = []) {
        $data = ['contents' => $html_content, 'settings' => $settings];
        return $this->post('/api/pdf/html-pdf', $data);
    }

    public function htmlToImage(string $html_content, array $settings = []) {
        $data = ['contents' => $html_content, 'settings' => $settings];
        return $this->post('/api/pdf/html-image', $data);
    }

    public function urlToImage(string $url, array $settings = []) {
        $data = ['url' => $url, 'settings' => $settings];
        return $this->post('/api/pdf/html-image', $data);
    }

    public function textOcr(string $image, string $type, array $settings = []) {
        $imageBase64 = $this->imgToBase64($image);
        $data = ['image' => $imageBase64, 'settings' => $settings];
        return $this->post("/api/text-ocr/{$type}", $data);
    }

    public function textOcrBankCard(string $image, array $settings = []) {
        $imageBase64 = $this->imgToBase64($image);
        $data = ['image' => $imageBase64, 'settings' => $settings];
        return $this->post('/api/text-ocr/bankcard ', $data);
    }

    public function textOcrIdCardFront(string $image, array $settings = []) {
        $imageBase64 = $this->imgToBase64($image);
        $data = ['image' => $imageBase64, 'settings' => $settings];
        return $this->post('/api/text-ocr/idcard-front', $data);
    }

    public function textOcrIdCardBack(string $image, array $settings = []) {
        $imageBase64 = $this->imgToBase64($image);
        $data = ['image' => $imageBase64, 'settings' => $settings];
        return $this->post('/api/text-ocr/idcard-back', $data);
    }

    public function mobileRecognize(string $mobile, array $settings = []) {
        $data = ['mobile' => $mobile, 'settings' => $settings];
        return $this->post('/api/mobile/recognize', $data);
    }

    public function coordinateBD09ToGCJ02(array $coordinates = [], $order = 'latlon') {
        $data = ['coordinates' => $coordinates, 'order' => $order];
        return $this->post('/api/coordinate/bd09-gcj02', $data);
    }

    public function coordinateBD09ToWGS84(array $coordinates = [], $order = 'latlon') {
        $data = ['coordinates' => $coordinates, 'order' => $order];
        return $this->post('/api/coordinate/bd09-wgs84', $data);
    }

    public function coordinateGCJ02ToWGS84(array $coordinates = [], $order = 'latlon') {
        $data = ['coordinates' => $coordinates, 'order' => $order];
        return $this->post('/api/coordinate/gcj02-wgs84', $data);
    }

    public function coordinateGCJ02ToBD09(array $coordinates = [], $order = 'latlon') {
        $data = ['coordinates' => $coordinates, 'order' => $order];
        return $this->post('/api/coordinate/gcj02-bd09', $data);
    }

    public function coordinateWGS84ToGCJ02(array $coordinates = [], $order = 'latlon') {
        $data = ['coordinates' => $coordinates, 'order' => $order];
        return $this->post('/api/coordinate/wgs84-gcj02', $data);
    }

    public function coordinateWGS84ToBD09(array $coordinates = [], $order = 'latlon') {
        $data = ['coordinates' => $coordinates, 'order' => $order];
        return $this->post('/api/coordinate/wgs84-bd09', $data);
    }

}
