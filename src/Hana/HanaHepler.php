<?php

namespace Fstar\Client\Hana;

use Fstar\Client\Constants;
use Fstar\Client\Hana\HanaConstants;
use Fstar\Client\Hana\HanaException;
use Fstar\Client\LogService;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use DB;

class HanaHepler {

    protected $conf;
    private $client;

    function __construct($config = []) {
        $config = empty($config) ? config('fstar-client') : $config;
        $conn_name = data_get($config, 'db_conn_base');
        $conn = DB::connection($conn_name);
        $this->conf = $conn->table('hana_conf as hc')->join('host_conf as h', 'h.host_conf_id', '=', 'hc.conf_host')
                           ->where('hc.delete_flag', Constants::DEL_NO)
                           ->get(['hc.*', 'h.host_name'])
                           ->pluck(null, 'conf_key')
                           ->toArray();
        $this->client = new Client();
        $this->log = new LogService('hana', data_get($config, 'hana_write_log', true));
    }

    protected function post($key, $data = []) {
        $url = $this->dealAuthInfo($key, $data);
        $param_str = is_array($data) ? json_encode($data) : $data;
        $this->log->info("Req url:{$url} params:{$param_str}");
        $resp = $this->client->post($url, [
            'form_params' => $data
        ]);
        return $this->parseResp($resp);
    }

    protected function get($key, $data = []) {
        $url = $this->dealAuthInfo($key, $data);
        $param_str = is_array($data) ? json_encode($data) : $data;
        $this->log->info("Req url:{$url} params:{$param_str}");
        $resp = $this->client->get($url, [
            'query' => $data
        ]);
        return $this->parseResp($resp);
    }

    protected function toJsonStr($obj) {
        if(is_array($obj)) {
            $obj = json_encode($obj);
        }
        return $obj;
    }

    private function parseResp(ResponseInterface $resp) {
        $content = $resp->getBody()->getContents();
        $this->log->info("Resp body:{$content}");
        $data = json_decode($content, true);
        if($data['result'] != HanaConstants::SUCCESS) {
            throw new HanaException($data['errmsg'], $data['errcode']);
        }
        if(isset($data['detail'])) {
            return ['detail' => $data['detail'], 'data' => $data['data']];
        }
        return $data['data'];
    }

    private function dealAuthInfo($key, &$data) {
        if(!array_key_exists($key, $this->conf)) {
            throw new HanaException("hana配置不存在 配置key:{$key}");
        }
        $conf = $this->conf[$key];
        $data['token'] = $conf->conf_token;
        return "{$conf->host_name}{$conf->conf_url}";
    }

}
