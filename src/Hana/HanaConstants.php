<?php

namespace Fstar\Client\Hana;

class HanaConstants {
    const SUCCESS = 1;
    const ERROR = 0;

    const DEAL_TYPE_SALE = '出售';
    const DEAL_TYPE_RENT = '出租';

    const PA_TYPE_BLOCK = 'B';

    const COVERABLE_NO = 0;
    const COVERABLE_YES = 1;

    const TYPE_DIC = 'E';
    const TYPE_CITY = 'T';
    const TYPE_DISTRICT = 'D';
    const TYPE_SCHOOL = 'S';
    const TYPE_BLOCK = 'D';
}