<?php

namespace Fstar\Client\Hana;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\Hana\HanaService
 *
 * @method static HanaService newInstanse()
 * @method static getDicDealPrice(string|integer $city_id, string $start_date, string $end_date, string $deal_type = HanaConstants::DEAL_TYPE_SALE, string $type = HanaConstants::TYPE_DIC)
 * @method static getDicClass(string|integer $city_id, string $start_date, string $end_date)
 * @method static getDicPrice(string|integer $city_id, string $start_date, string $end_date, string $type = HanaConstants::TYPE_DISTRICT, string $dics = '')
 * @method static getHsCntByPolygon(string|integer $city_id, string $type = HanaConstants::TYPE_SCHOOL)
 * @method static getHsCntByLocation(string|integer $city_id, string|array $locations, string|integer $radius, string $type = HanaConstants::TYPE_SCHOOL)
 * @method static queryShopLocation(float|string $lat, float|string $lng, int $dist = 100000, int $city_id = 1)
 * @method static delSchoolScope(int $school_id)
 * @method static querySchoolScopeDic(int $school_id, int $radius)
 * @method static saveSchoolScope(int $school_id, string|array $scope_data)
 * @method static calcCentroidByPoints(string|array $points)
 * @method static getDicTags(int $city_id, string $type, array $key_arry)
 * @method static getBlockPolygonArea(int $city_id)
 * @method static getThreeDeptPolygonArea(int $city_id)
 * @method static getHighQualityHouse(int $city_id)
 * @method static getReportList()
 * @method static getOneOffAnalysisUrl(string $username, string $view, string $session_id, string $origin = 'erp')
 * @method static polygonAdd(string $name, string $polygon, string $type = HanaConstants::PA_TYPE_BLOCK, int $coverable = HanaConstants::COVERABLE_YES, int $operator = Constants::SYS_EMP_ID)
 * @method static polygonModify(string $id, int $city_id, string $name, string $polygon, string $type = HanaConstants::PA_TYPE_BLOCK, int $coverable = HanaConstants::COVERABLE_YES, int $operator = Constants::SYS_EMP_ID)
 * @method static polygonDel(string $id, string $type = HanaConstants::PA_TYPE_BLOCK, int $operator = Constants::SYS_EMP_ID)
 * @method static post(string $key, array $params)
 * @method static get(string $key, array $params)
 */
class HanaAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_hana;
    }
}