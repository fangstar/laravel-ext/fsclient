<?php

namespace Fstar\Client\Hana;

use Fstar\Client\Constants;
use Fstar\Client\Hana\HanaHepler;
use Fstar\Client\LogService;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use DB;

class HanaService extends HanaHepler {

    public function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * 获取楼盘成交价
     *
     * @param string|integer $city_id    城市ID
     * @param string         $start_date 开始日期  YYYY-MM-DD
     * @param string         $end_date   结束日期  YYYY-MM-DD
     * @param string         $type       类型  'D' 行政区 'T' 整个城市 'E' 新盘
     *
     * @return mixed
     */
    public function getDicDealPrice($city_id, $start_date, $end_date, $deal_type = HanaConstants::DEAL_TYPE_SALE, $type = HanaConstants::TYPE_DIC) {
        $data = ['city_id' => $city_id, 'type' => $type, 'start_date' => $start_date, 'end_date' => $end_date, 'deal_type' => $deal_type];
        return $this->get('GET_DEAL_PRICE', $data);
    }

    /**
     * 获取楼盘评级
     *
     * @param string|integer $city_id    城市ID
     * @param string         $start_date 开始日期  YYYY-MM-DD
     * @param string         $end_date   结束日期  YYYY-MM-DD
     *
     * @return mixed
     */
    public function getDicClass($city_id, $start_date, $end_date) {
        $data = ['city_id' => $city_id, 'start_date' => $start_date, 'end_date' => $end_date];
        return $this->get('GET_DIC_CLASS', $data);
    }

    /**
     * 获取楼盘挂牌价
     *
     * @param string|integer $city_id    城市ID
     * @param string         $start_date 开始日期  YYYY-MM-DD
     * @param string         $end_date   结束日期  YYYY-MM-DD
     * @param string         $type       类型  'D' 行政区 'T' 整个城市 'E' 新盘
     * @param string|array   $dics       [{"key":"6161"},{"key":"3051"}]
     */
    public function getDicPrice($city_id, $start_date, $end_date, $type = HanaConstants::TYPE_DISTRICT, $dics = '') {
        $dics = is_array($dics) ? json_encode($dics) : $dics;
        $data = ['city_id' => $city_id, 'start_date' => $start_date, 'type' => $type, 'end_date' => $end_date, 'dics' => $dics];
        return $this->post('GET_DIC_PRICE', $data);
    }


    /**
     * 根据多边形范围获取周边在售在租房源数
     *
     * @param string|integer $city_id 城市ID
     * @param string         $type    'S' 学区；'D' 商圈
     */
    public function getHsCntByPolygon($city_id, $type = HanaConstants::TYPE_SCHOOL) {
        $data = ['city_id' => $city_id, 'cmd' => 'polygon', 'type' => $type];
        return $this->get('GET_HS_CNT_BY_LOCATION', $data);
    }

    /**
     * 根据位置指定半径区域获取周边在售在租房源数
     *
     * @param string|integer $city_id   城市ID
     * @param string|array   $locations [{"id":6161,"attrib1":经度,"attrib2":维度}]
     * @param string|integer $radius    半径（米）
     * @param string         $type      'S' 学区；'D' 商圈
     */
    public function getHsCntByLocation($city_id, $locations, $radius, $type = HanaConstants::TYPE_SCHOOL) {
        $locations = is_array($locations) ? json_encode($locations) : $locations;
        $data = ['city_id' => $city_id, 'cmd' => 'location', 'type' => $type, 'locations' => $locations, 'radius' => $radius];
        return $this->post('GET_HS_CNT_BY_LOCATION', $data);
    }

    /**
     * 查询周边店面位置
     *
     * @param string|float $lat
     * @param string|float $lng
     * @param int          $dist
     * @param int          $city_id
     *
     * @throws HanaException
     */
    public function queryShopLocation($lat, $lng, $dist = 100000, $city_id = 1) {
        $data = ['latitude' => $lat, 'longitude' => $lng, 'city' => $city_id, 'dist' => $dist];
        return $this->post('QUERY_SHOP_LOCATION', $data);
    }

    /**
     * 删除学校划片
     *
     * @param int $school_id
     *
     * @throws HanaException
     */
    public function delSchoolScope($school_id) {
        $data = ['school_id' => $school_id, 'cmd' => 'delScopeBySchoolId'];
        return $this->post('DRAW_SCHOOL_SCOPE', $data);
    }

    /**
     * 获取学校划片范围内的楼盘数据
     *
     * @param int $school_id
     * @param int $radius
     *
     * @throws HanaException
     */
    public function querySchoolScopeDic($school_id, $radius) {
        $data = ['school_id' => $school_id, 'radius' => $radius, 'cmd' => 'getDicBySchoolScope'];
        return $this->post('DRAW_SCHOOL_SCOPE', $data);
    }

    /**
     * 保存学校划片数据
     *
     * @param int          $school_id
     * @param string|array $scope_data
     *
     * @throws HanaException
     */
    public function saveSchoolScope($school_id, $scope_data) {
        $data = ['school_id' => $school_id, 'data' => $this->toJsonStr($scope_data), 'cmd' => 'saveSchoolScope'];
        return $this->post('DRAW_SCHOOL_SCOPE', $data);
    }


    /**
     * 计算坐标中心点
     *
     * @param string|array $points
     *
     * @throws HanaException
     */
    public function calcCentroidByPoints($points) {
        $data = ['points' => $this->toJsonStr($points), 'cmd' => 'calcCentroidByPoints'];
        return $this->post('DRAW_SCHOOL_SCOPE', $data);
    }

    /**
     * 获取楼盘标签
     *
     * @param int    $city_id
     * @param string $type
     * @param array  $key_arry
     *
     * @throws HanaException
     */
    public function getDicTags($city_id, $type, $key_arry) {
        $keys = [];
        foreach($key_arry as $key) {
            $keys[] = ['key' => $key];
        }
        $data = ['city_id' => $city_id, 'type' => $type, 'keys' => $this->toJsonStr($keys)];
        return $this->post('GET_DIC_TAGS', $data, null);
    }

    /**
     * 获取商圈矩形区域范围数据
     *
     * @param int $city_id
     *
     * @throws HanaException
     */
    public function getBlockPolygonArea($city_id) {
        $data = ['city_id' => $city_id, 'pa_type' => 'B'];
        return $this->post('POLYGON_AREA', $data);
    }

    /**
     * 获取尊园大区矩形区域范围数据
     *
     * @param int $city_id
     *
     * @throws HanaException
     */
    public function getThreeDeptPolygonArea($city_id) {
        $data = ['city_id' => $city_id, 'pa_type' => 'Z'];
        return $this->post('POLYGON_AREA', $data);
    }

    /**
     * 获取推荐房源
     *
     * @param int $city_id
     *
     * @throws HanaException
     */
    public function getHighQualityHouse($city_id) {
        $data = ['city_id' => $city_id];
        return $this->post('GET_HIGH_QUALITY_HS', $data);
    }

    /**
     * 查询OLAP分析列表
     *
     * @throws HanaException
     */
    public function getReportList() {
        $data = [];
        return $this->post('REPORT_LIST', $data);
    }

    /**
     * @param string $username
     * @param string $view
     * @param string $session_id
     * @param string $origin
     */
    public function getOneOffAnalysisUrl($username, $view, $session_id, $origin = 'erp') {
        $data = ['username' => $username, 'view' => $view, 'session_id' => $session_id, 'origin' => 'erp'];
        $key = 'ONE_OFF_ANALYSIS_URL';
        $result = $this->post($key, $data);
        $conf = $this->conf[$key];
        return "{$conf->host_name}{$result['url']}?mod={$result['mod']}&view={$result['view']}&token={$result['token']}&auth_type={$result['auth_type']}&username={$username}&origin={$origin}";
    }

    /**
     * 添加矩形
     *
     * @param string $name
     * @param string $polygon
     * @param string $type
     * @param int    $coverable
     * @param int    $operator
     *
     * @return mixed|null
     * @throws HanaException
     */
    public function polygonAdd($name, $polygon, $type = HanaConstants::PA_TYPE_BLOCK, $coverable = HanaConstants::COVERABLE_YES, $operator = Constants::SYS_EMP_ID) {
        $data = [
            'cmd'       => 'addNewPolygon',
            'pa_name'   => $name,
            'pa_type'   => $type,
            'coverable' => $coverable,
            'polygon'   => $this->toJsonStr($polygon),
            'operator'  => $operator
        ];
        return $this->post('DRAW_POLYGON', $data);
    }

    /**
     * 修改矩形
     *
     * @param string $id
     * @param string $name
     * @param string $polygon
     * @param string $type
     * @param int    $coverable
     * @param int    $operator
     *
     * @throws HanaException
     */
    public function polygonModify($id, $city_id, $name, $polygon, $type = HanaConstants::PA_TYPE_BLOCK, $coverable = HanaConstants::COVERABLE_YES, $operator = Constants::SYS_EMP_ID) {
        $data = [
            'cmd'       => 'modifyPolygon',
            'paid'      => $id,
            'city_id'   => $city_id,
            'pa_name'   => $name,
            'pa_type'   => $type,
            'coverable' => $coverable,
            'polygon'   => $this->toJsonStr($polygon),
            'operator'  => $operator
        ];
        return $this->post('DRAW_POLYGON', $data);
    }

    /**
     * 删除矩形区域
     *
     * @param string $id
     * @param string $type
     * @param int    $operator
     *
     * @throws HanaException
     */
    public function polygonDel($id, $type = HanaConstants::PA_TYPE_BLOCK, $operator = Constants::SYS_EMP_ID) {
        $data = [
            'cmd'      => 'deletePolygon',
            'paid'     => $id,
            'pa_type'  => $type,
            'operator' => $operator
        ];
        return $this->post('DRAW_POLYGON', $data);
    }

}
