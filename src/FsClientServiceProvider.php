<?php

namespace Fstar\Client;

use Fstar\Client\EagleEye\EagleEyeService;
use Fstar\Client\ErpBase\ErpBaseService;
use Fstar\Client\FsFinance\FsFinanceService;
use Fstar\Client\FsPay\FsPayService;
use Fstar\Client\Hana\HanaService;
use Fstar\Client\Mongo\MongoService;
use Fstar\Client\Msgc\Im\MsgcImUserService;
use Fstar\Client\Msgc\MsgcCaptchaService;
use Fstar\Client\Msgc\MsgcService;
use Fstar\Client\Protyle\ProtyleService;
use Illuminate\Support\ServiceProvider;

class FsClientServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {
        // Publish the configuration file
        $this->publishes([__DIR__."/config/fstar-client.php" => config_path(Constants::conf_name.".php")]);

    }

    /**
     * 在容器中注册绑定。
     *
     * @return void
     */
    public function register() {
        $this->mergeConfigFrom(__DIR__."/config/fstar-client.php", Constants::conf_name);
        $user_config = $this->app['config'][Constants::conf_name];
        $this->app->singleton(Constants::lib_msgc, function($app) use ($user_config) {
            return new MsgcService($user_config);
        });
        $this->app->singleton(Constants::lib_msgc_im, function($app) use ($user_config) {
            return new MsgcImUserService($user_config);
        });
        $this->app->singleton(Constants::lib_msgc_captcha, function($app) use ($user_config) {
            return new MsgcCaptchaService($user_config);
        });
        $this->app->singleton(Constants::lib_hana, function($app) use ($user_config) {
            return new HanaService($user_config);
        });
        $this->app->singleton(Constants::lib_protyle, function($app) use ($user_config) {
            return new ProtyleService($user_config);
        });
        $this->app->singleton(Constants::lib_eagleeye, function($app) use ($user_config) {
            return new EagleEyeService($user_config);
        });
        $this->app->singleton(Constants::lib_mongo_svc, function($app) use ($user_config) {
            return new MongoService($user_config);
        });
        $this->app->singleton(Constants::lib_erp_base, function($app) use ($user_config) {
            return new ErpBaseService($user_config);
        });
        $this->app->singleton(Constants::lib_fspay, function($app) use ($user_config) {
            return new FsPayService($user_config);
        });
        $this->app->singleton(Constants::lib_fs_finance, function($app) use ($user_config) {
            return new FsFinanceService($user_config);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return [
            Constants::lib_msgc,
            Constants::lib_msgc_im,
            Constants::lib_msgc_captcha,
            Constants::lib_hana,
            Constants::lib_protyle,
            Constants::lib_eagleeye,
            Constants::lib_mongo_svc,
            Constants::lib_erp_base,
            Constants::lib_fspay,
            Constants::lib_fs_finance,
        ];
    }
}