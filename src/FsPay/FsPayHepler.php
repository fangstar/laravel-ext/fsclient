<?php

namespace Fstar\Client\FsPay;

use Fstar\Client\LogService;
use GuzzleHttp\Client;

class FsPayHepler {

    private $PAY_USER_NO = null;
    private $PAY_TOKEN = null;
    private $PAY_HOST = null;
    private $log = null;

    function __construct($config = []) {
        $config = empty($config) ? config('fstar-client') : $config;
        $this->PAY_HOST = data_get($config, 'fspay_host');
        $this->PAY_TOKEN = data_get($config, 'fspay_token');
        $this->PAY_USER_NO = data_get($config, 'fspay_user_no');
        $this->log = new LogService('pay', data_get($config, 'fspay_write_log', true));
    }

    protected function parseReceiver($receiver) {
        if(!empty($receiver) && is_array($receiver)) {
            return json_encode($receiver);
        }
        return $receiver;
    }

    protected function getHeaders($now, $data) {
        $auth = $this->PAY_USER_NO.';'.md5($now.$this->PAY_TOKEN.$data);
        return ['Content-Type' => 'application/json', 'Authorization' => $auth, 'Date' => $now];
    }

    protected function post($path, $data) {
        $now = time();
        $data_str = is_string($data) ? $data : json_encode($data);
        $headers = $this->getHeaders($now, $data_str);
        $url = "{$this->PAY_HOST}{$path}";
        $this->log->info("Req url:{$url} params:{$data_str}");
        try {
            $response = (new Client())->request('POST', $url, [
                'headers'         => $headers,
                'timeout'         => 60,
                'connect_timeout' => 5,
                'verify'          => false,
                'body'            => $data_str
            ]);
            $content = $response->getBody()->getContents();
            $this->log->info("Resp body:{$content}");
            if(!is_array($content)) {
                $content = json_decode($content, true);
            }
            return $content;
        } catch(\Exception $ex) {
            throw new FsPayException("消息中心接口请求失败");
        }
    }
}
