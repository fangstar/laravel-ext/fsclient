<?php

namespace Fstar\Client\FsPay;

class FsPayConstants {
    const CHANNEL_WECHAT = 'wechat';
    const CHANNEL_ALIPAY = 'ali';
}