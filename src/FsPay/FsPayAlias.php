<?php

namespace Fstar\Client\FsPay;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\FsPay\FsPayService
 *
 * @method static FsPayService newInstanse()
 * @method static array scanQrPayOrder(string $channel, string $out_trade_no, string $app_id, string $desc, integer $amount, string $notify_url)
 */
class FsPayAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_fspay;
    }
}