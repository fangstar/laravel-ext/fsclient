<?php

namespace Fstar\Client\FsPay;

class FsPayService extends FsPayHepler {
    function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * 扫码支付下单 获取支付二维码
     *
     * @param string  $channel      支付渠道 ali | wechat
     * @param string  $out_trade_no 商户交易号
     * @param string  $app_id       APP ID
     * @param string  $desc         商品说明
     * @param integer $amount       交易金额，单位:分
     * @param string  $notify_url   支付结果回调地址
     *
     * @return array 支付二维码
     * @throws FsPayException
     */
    public function scanQrPayOrder($channel, $out_trade_no, $app_id, $desc, $amount, $notify_url) {
        $now = time();
        $path = '/api/native-pay/scan-qr-order';
        $data = ['channel' => $channel, 'out_trade_no' => $out_trade_no, 'app_id' => $app_id, 'desc' => $desc, 'amount' => $amount, 'notify_url' => $notify_url];
        return $this->post($path, $data);
    }

}
