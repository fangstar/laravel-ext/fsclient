<?php

namespace Fstar\Client\Protyle;

use Fstar\Client\Constants;
use Fstar\Client\LogService;
use GuzzleHttp\Client;

class ProtyleHelper {
    private $host    = null;
    private $domain  = null;
    private $catalog = null;
    private $log     = null;

    function __construct($config = []) {
        $config        = empty($config) ? config('fstar-client') : $config;
        $this->host    = data_get($config, 'protyle_url');
        $this->domain  = data_get($config, 'protyle_domain');
        $this->catalog = data_get($config, 'protyle_catelog');
        $this->log     = new LogService('protyle', data_get($config, 'protyle_write_log', true));
    }


    protected function request($path, $params, $method = 'GET') {
        $url            = "{$this->host}{$path}";
        $default_params = [
            'catalog' => $this->catalog,
            'uid'     => session('emp_id', Constants::SYS_EMP_ID),
            'ip'      => '',
            'domain'  => $this->domain,
            'code'    => $this->getCode()
        ];
        $req_params     = array_merge($default_params, $params);
        $param_str      = json_encode($req_params);
        try {
            $response = (new Client())->request($method, $url, [
                'query'           => $req_params,
                'timeout'         => 30,
                'connect_timeout' => 5,
                'verify'          => false
            ]);
            $content  = $response->getBody()->getContents();
            $this->log->info("Req url:{$url} params:{$param_str} Resp body:{$content}");
            return json_decode($content, true);
        } catch(\Exception $ex) {
            $this->log->info("Req url:{$url} params:{$param_str} Error:{$ex->getMessage()}");
            throw $ex;
        }
    }

    protected function post($path, $data, $method = 'POST') {
        $url        = "{$this->host}{$path}";
        $data_str   = is_array($data) ? json_encode($data) : $data;
        $req_params = [
            'catalog' => $this->catalog,
            'uid'     => session('emp_id', Constants::SYS_EMP_ID),
            'ip'      => '',
            'domain'  => $this->domain,
            'code'    => $this->getCode()
        ];
        $param_str  = json_encode($req_params);
        try {
            $response = (new Client())->request($method, $url, [
                'query'           => $param_str,
                'json'            => $data,
                'timeout'         => 30,
                'connect_timeout' => 5,
                'verify'          => false
            ]);
            $content  = $response->getBody()->getContents();
            $this->log->info("Req url:{$url} query:{$param_str} data:{$data_str} Resp body:{$content}");
            return json_decode($content, true);
        } catch(\Exception $ex) {
            $this->log->info("Req url:{$url} query:{$param_str} data:{$data_str} Error:{$ex->getMessage()}");
            throw $ex;
        }
    }

    private function getCode() {
        $trace_arry = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 3);
        unset($trace_arry[0]);
        foreach($trace_arry as $trace) {
            if(isset($trace['file']) && isset($trace['line'])) {
                return "{$trace['file']}:{$trace['line']}";
            }
        }
        return '';
    }
}
