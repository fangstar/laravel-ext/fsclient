<?php

namespace Fstar\Client\Protyle;


class ProtyleService extends ProtyleHelper {
    public function __construct($config) {
        parent::__construct($config);
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * 获取原值
     *
     * @param $uuid
     *
     * @throws ProtyleException
     */
    public function getRaw($uuid, $default = null) {
        $value  = null;
        $path   = 'retrieve';
        $params = ['uuid' => $uuid];
        $data   = $this->request($path, $params);
        if($data['result'] == ProtyleConstants::SUCCESS) {
            $value = $data['data']['value'];
        } else {
            if($data['result'] != 1) {
                throw new ProtyleException("Protyle 请求参数校验异常");
            }
        }
        if($value == null) {
            return $default;
        }
        return $value;
    }


    /**
     * 获取UUID，没有则自动创建
     *
     * @param $value
     *
     * @throws ProtyleException
     */
    public function getUuid($value, $default = null) {
        $uuid   = null;
        $path   = 'uuid';
        $params = ['value' => $value];
        $data   = $this->request($path, $params);
        if($data['result'] == ProtyleConstants::SUCCESS) {
            $uuid = $data['data']['uuid'];
        } else {
            if($data['result'] != 1) {
                throw new ProtyleException("Protyle 请求参数校验异常");
            }
        }
        if($uuid == null) {
            return $default;
        }
        return $uuid;
    }

    /**
     * 查询UUID，没有则自动创建
     *
     * @param $value
     */
    public function getOrCreate($value) {
        $uuid = $this->getUuid($value);
        if(is_null($uuid)) {
            $uuid = $this->create($value);
        }
        return $uuid;
    }

    /**
     * 创建
     *
     * @param $value
     */
    public function create($value) {
        $uuid   = null;
        $path   = 'create';
        $params = ['value' => $value];
        $data   = $this->request($path, $params);
        if($data['result'] == ProtyleConstants::SUCCESS) {
            $uuid = $data['data']['uuid'];
        } else {
            if($data['result'] == 1) {
                throw new ProtyleException("Protyle 内部服务异常");
            } else {
                throw new ProtyleException("Protyle 请求参数校验异常");
            }
        }
        return $uuid;
    }

    public function phoneSectionInfoByUuid(array $uuids) {
        $path = 'phone-section-info';
        $data = $this->post($path, $uuids);
        if($data['result'] == ProtyleConstants::SUCCESS) {
            return $data['data'];
        } else {
            throw new ProtyleException("Protyle 内部服务异常,{$data['msg']}");
        }
    }

    public function phoneSectionInfoByMobile(array $mobiles) {
        $path = 'phone-info-by-section';
        $data = $this->post($path, $mobiles);
        if($data['result'] == ProtyleConstants::SUCCESS) {
            return $data['data'];
        } else {
            throw new ProtyleException("Protyle 内部服务异常,{$data['msg']}");
        }
    }
}
