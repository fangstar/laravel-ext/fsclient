<?php

namespace Fstar\Client\Protyle;

use Fstar\Client\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Client\Protyle\ProtyleService
 *
 * @method static ProtyleService newInstanse()
 * @method static getRaw(string $uuid, string $default = null)
 * @method static getUuid(string $value, string $default = null)
 * @method static getOrCreate(string $value)
 * @method static create(string $value)
 * @method static phoneSectionInfoByUuid(array $uuids)
 * @method static phoneSectionInfoByMobile(array $mobiles)
 */
class ProtyleAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_protyle;
    }
}