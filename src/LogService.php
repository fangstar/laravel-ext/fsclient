<?php

namespace Fstar\Client;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LogService {
    private $monolog = null;
    private $write_log = true;

    public function __construct($type, $write_log = true, $name = 'local') {
        $this->write_log = $write_log;
        $this->monolog = new Logger($name);
        $dir_sep = DIRECTORY_SEPARATOR;
        $date = date('Ymd');
        $streamHandler = new StreamHandler(storage_path("logs{$dir_sep}fsclient{$dir_sep}{$type}-{$date}.log"), Logger::INFO);
        $this->monolog->pushHandler($streamHandler);
    }

    public function info($msg) {
        if ($this->write_log) {
            $this->monolog->info($msg);
        }
    }

    public function warn($msg) {
        if ($this->write_log) {
            $this->monolog->warn($msg);
        }
    }

    public function error($msg) {
        if ($this->write_log) {
            $this->monolog->error($msg);
        }
    }
}