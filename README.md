# Fstar Client

#### 介绍
房星科技客户端组件

#### 软件架构
laravel


#### 安装教程

1. composer require fstar/client
2. php artisan vendor:publish --provider=Fstar\Client\FsClientServiceProvider
3. .env 文件增加配置
``` php
   DB_CONN_BASE=base 库连接
   HANA_WRITE_LOG=true

   MSGC_HOST=${msgc_host}
   MSGC_TOKEN=${msgc_token}
   MSGC_USER_NO=${msgc_user_no}
   MSGC_WRITE_LOG=true
   
   PROTYLE_URL=${protyle_server_url}
   PROTYLE_DOMAIN=ERP
   PROTYLE_CATALOG=global
   PROTYLE_WRITE_LOG=true
   
   EAGLE_EYE_HOST=${eagle_eye_host}
   EAGLE_EYE_TOKEN=${eagle_eye_token}
   EAGLE_EYE_USER_NO=${eagle_eye_user_no}
   EAGLE_EYE_WRITE_LOG=true
   
   MONGODB_SVC_URL=
   MONGODB_SVC_WRITE_LOG=false
   
   ERP_BASE_URL=
   ERP_BASE_WRITE_LOG=true
   
   FS_FINANCE_HOST=${fs_finance_host}
   FS_FINANCE_USER_NO=${fs_finance_token}
   FS_FINANCE_TOKEN=${fs_finance_user_no}
   FS_FINANCE_WRITE_LOG=true
   
```

#### 使用说明
###### 消息中心接口
``` php
FsMsgc::newInstanse()
FsMsgc::sendSms(string $receiver, string $message, string $sms_app_key = Constants::ZHOME_SMS_DEFAULT)
FsMsgc::sendSmssendWechatText(string $app_key, string $receiver_type, string $content, string $receiver = "")
FsMsgc::sendSmssendWechatTextCard(string $app_key, string $receiver_type, string $title, string $content, string $open_url, string $receiver = "")
FsMsgc::sendSmssendWechatNews(string $app_key, string $receiver_type, string $title, string $content, string $open_url, string $img_url, string $receiver = "")
FsMsgc::sendSmssendWechatMarkdown(string $app_key, string $receiver_type, string $content, string $receiver = "")
FsMsgc::sendSmssendWechatImage(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
FsMsgc::sendSmssendWechatVoice(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
FsMsgc::sendSmssendWechatVideo(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
FsMsgc::sendSmssendWechatFile(string $app_key, string $receiver_type, string $media_url, string $receiver = "")
FsMsgc::sendSmssendWechatCustom(string $app_key, string $receiver_type, string $custom_content, string $receiver = "")
FsMsgc::sendSmsgetOpenIdByEmpId(string|integer $emp_id, string $app_key = Constants::ZHOME_PHONE_BOOK)

FsMsgc::sendImTextMsg(string|integer|array $receiver, string $content, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
FsMsgc::sendImImageMsg(string|integer|array $receiver, string $img_url, integer $img_width, integer $img_height, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
FsMsgc::sendImFileMsg(string|integer|array $receiver, string $file_url, string $file_name, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
FsMsgc::sendImVideoMsg(string|integer|array $receiver, string $video_url, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
FsMsgc::sendImAudioMsg(string|integer|array $receiver, string $audio_url, integer $audio_second, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
FsMsgc::sendImLocationMsg(string|integer|array $receiver, string $content, float $lat, float $lng, string|integer $sender = null, string $sync_other_machine = null, integer $receiver_type = null, string $im_app_key = null)
FsMsgc::sendImCustomMsg(string|integer|array $receiver, string $title, string $content, array|string $data, string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
FsMsgc::sendImZhomeH5Msg(string|integer|array $receiver, string $title, string $content, string $open_url, string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
FsMsgc::sendImZhomeUniappMsg(string|integer|array $receiver, string $title, string $content, string $node_key, string $open_url, string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
FsMsgc::sendImZhomeCustMsg(string|integer|array $receiver, string $title, string $content, string|integer $cust_id, array $extra_params = [], string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
FsMsgc::sendImZhomeHsSaleMsg(string|integer|array $receiver, string $title, string $content, string $hs_id, array $extra_params = [], string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)
FsMsgc::sendImZhomeHsRentMsg(string|integer|array $receiver, string $title, string $content, string $hs_id, array $extra_params = [], string|integer $sender = null, string $sync_other_machine = null, $ext_info = null, $receiver_type = null, $im_app_key = null)

FsMsgcImUser::newInstanse()
FsMsgcImUser::login($user_id, $user_name, $avatar = '', $gender = MsgcConstants::IM_USER_GENDER_UNKNOW, $im_app_key = null)
FsMsgcImUser::getUserByUserIds(array $user_ids, string $im_app_key = null)
FsMsgcImUser::getUserByUserId(string|integer $user_id, string $im_app_key = null)
FsMsgcImUser::getUserByImIds(string $im_ids, string $im_app_key = null)
FsMsgcImUser::getUserByImId(string $im_id, string $im_app_key = null)
FsMsgcImUser::createGroup(array $content)
FsMsgcImUser::modifyGroupBaseInfo(array $content)
FsMsgcImUser::destroyGroup(mixed $group_id)
FsMsgcImUser::importGroupMember(array $content)
FsMsgcImUser::removeGroupMember(mixed $group_id, array $im_ids, integer $silence = 0)
FsMsgcImUser::modifyGroupMemberInfo(array $content)
 
FsMsgcCaptcha::newInstanse()
FsMsgcCaptcha::generate(string $type = MsgcConstants::CAPTCHA_TYPE_SLIDER, string $img_group = null)
FsMsgcCaptcha::matching(string $id, array $captcha_track, boolean $open_two_step_valid = false, int $two_step_valid_overdue = 300)
FsMsgcCaptcha::tokenValid(string $token)

```
###### HANA 接口
``` php
FsHana::newInstanse()
FsHana::getDicDealPrice(string|integer $city_id, string $start_date, string $end_date, string $deal_type = '出售', string $type = HanaConstants::TYPE_DIC)
FsHana::getDicClass(string|integer $city_id, string $start_date, string $end_date)
FsHana::getDicPrice(string|integer $city_id, string $start_date, string $end_date, string $type = HanaConstants::TYPE_DISTRICT, string $dics = '')
FsHana::getHsCntByPolygon(string|integer $city_id, string $type = HanaConstants::TYPE_SCHOOL)
FsHana::getHsCntByLocation(string|integer $city_id, string|array $locations, string|integer $radius, string $type = HanaConstants::TYPE_SCHOOL)

```
###### Protyle 接口
``` php
FsProtyle::newInstanse()
FsProtyle::getRaw(string $uuid, string $default = null)
FsProtyle::getUuid(string $value, string $default = null)
FsProtyle::getOrCreate(string $value)
FsProtyle::create(string $value)
FsProtyle::phoneSectionInfoByUuid(array $uuids)
FsProtyle::phoneSectionInfoByMobile(array $mobiles)

```
###### EagleEye 接口
``` php
FsEagleEye::newInstanse()
FsEagleEye::htmlToPdf(string $html_content, array $settings = [])
FsEagleEye::htmlToImage(string $html_content, array $settings = [])
FsEagleEye::urlToImage(string $html_content, array $settings = [])
FsEagleEye::textOcr(string $image, string $type, array $settings = []) //$image 为公网访问url或者图片base64
FsEagleEye::textOcrBankCard(string $image, array $settings = []) //$image 为公网访问url或者图片base64
FsEagleEye::textOcrIdCardFront(string $image, array $settings = []) //$image 为公网访问url或者图片base64
FsEagleEye::textOcrIdCardBack(string $image, array $settings = []) //$image 为公网访问url或者图片base64
FsEagleEye::mobileRecognize(string $mobile, array $settings = [])
FsEagleEye::coordinateBD09ToGCJ02(array $coordinates = [[lat,lon]], $order = 'latlon')
FsEagleEye::coordinateBD09ToWGS84(array $coordinates = [[lat,lon]], $order = 'latlon')
FsEagleEye::coordinateGCJ02ToWGS84(array $coordinates = [[lat,lon]], $order = 'latlon')
FsEagleEye::coordinateGCJ02ToBD09(array $coordinates = [[lat,lon]], $order = 'latlon')
FsEagleEye::coordinateWGS84ToGCJ02(array $coordinates = [[lat,lon]], $order = 'latlon')
FsEagleEye::coordinateWGS84ToBD09(array $coordinates = [[lat,lon]], $order = 'latlon')

```
###### Mongodb 服务接口
``` php
FsMongoSvc::newInstanse()
FsMongoSvc::taskLogRecordFindById(string $id)
FsMongoSvc::taskLogAddRecord(array $record)
FsMongoSvc::taskLogUpdRecord(array $record)
FsMongoSvc::taskLogFindRecord(array $params)
FsMongoSvc::taskLogAddLog(array $log)
FsMongoSvc::taskLogBatchAddLog(array $logs)
FsMongoSvc::taskLogFind(string $task_record_id)
FsMongoSvc::visitLogFind(string $mobile, int $min_time, int $cnt)
FsMongoSvc::visitLogAdd(string|array $params)
FsMongoSvc::visitLogUpdateTime(string|array $params)
FsMongoSvc::svcFlowLogAdd(string $svc_flow_instance_id, string $svc_flow_node_name, string $log_content, int $log_order, string $log_type = 'info')
FsMongoSvc::svcFlowLogFind(string $svc_flow_instance_id)
FsMongoSvc::fsAgentScoreFindByAgentId(string $agent_id)
FsMongoSvc::fsAgentScoreUpsert(string|array $params)
FsMongoSvc::fsRecommendFindByDicId(int $dic_id)
FsMongoSvc::fsRecommendFindByHouseType(int $house_type)
FsMongoSvc::fsRecommendUpsert(string|array $params)
FsMongoSvc::fsRecommendTruncate()
FsMongoSvc::phoneBrowseFind(string|array $params)
FsMongoSvc::phoneBrowseAdd(string|array $params)

```
###### Erp Base 接口
``` php
FsErpBase::newInstanse()
FsErpBase::post(string $path, string|array $data, array $params = ['timeout' => 30])

```

###### FsFinance 接口
``` php
FsFinance::newInstanse()
FsFinance::partnerSave(array $params)
FsFinance::partnerDel(string $src_object)
FsFinance::deptSave(array $params)
FsFinance::deptDel(string $src_object, boolean $include_child)
FsFinance::empSave(array $params)
FsFinance::empDel(string $src_object)
```

#### 参与贡献

1.  房星科技